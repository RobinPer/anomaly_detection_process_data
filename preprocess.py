import pandas as pd
import numpy as np
from joblib import Parallel, delayed
import os

def read_data_parallel(path='Masterflex-KI/Exporte', file='Seq-58.xlsx', save=True, use_diary=False):
    """
    Reads data from an Excel file specified by the 'path' parameter, with default value '/home/r948e/Downloads/Seq-58.xlsx'.
    The function extracts information from two sheets, 'bindings' and 'values', in the Excel file, and processes them in parallel.

    Parameters:
    - path (str): The file path of the Excel file to be read. Default is '/home/r948e/Downloads/Seq-58.xlsx'.

    Returns:
    None, but performs the following actions:
    - Reads 'bindings' and 'values' sheets from the specified Excel file into pandas DataFrames.
    - Converts the 'Value' column in the 'values' DataFrame to numeric, dropping rows with non-convertible values.
    - Sets the 'Time (Europe/Berlin)' column in the 'values' DataFrame as the index and rounds the time to the nearest second.
    - Creates a new DataFrame 'df_new' with unique rounded times as the index.
    - Processes each unique 'BindingId' value in parallel using the specified number of processes.
    - Fills in the 'df_new' DataFrame with corresponding values from the 'values' DataFrame for each time and BindingId.
    """

    # Get bindings
    df_bindings = pd.read_excel(os.path.join(path,file), sheet_name='bindings')
    df_value = pd.read_excel(os.path.join(path,file), sheet_name='values')

    if use_diary:
        print(df_value['Value'][df_value['BindingId'] == 'ba926976-de42-4acf-8b09-6db887c104b3'].values)
        logs = pd.read_excel('Masterflex-KI/MF_status.xlsx')
        index_to_change = []
        value_to_change = []
        for cur_stat in range(len(logs.displayname)):
            for cur_val in range(len(df_value['Value'][df_value['BindingId'] == 'ba926976-de42-4acf-8b09-6db887c104b3'].values)):
                if df_value['Value'][df_value['BindingId'] == 'ba926976-de42-4acf-8b09-6db887c104b3'].values[cur_val][1:-1] ==logs.displayname[cur_stat]:
                    print(df_value['Value'][df_value['BindingId'] == 'ba926976-de42-4acf-8b09-6db887c104b3'].values[cur_val][1:-1])
                    index_to_change.append(df_value[df_value['BindingId'] == 'ba926976-de42-4acf-8b09-6db887c104b3'].index[
                        cur_val])
                    print(index_to_change)
                    value_to_change.append(cur_stat/len(logs.displayname))

        for i in range(len(index_to_change)):
            df_value.loc[index_to_change[i],'Value'] = value_to_change[i]



    df_value['Value'] = pd.to_numeric(df_value['Value'], errors='coerce')
    # Drop rows where the 'Value' column could not be converted to float
    df_value = df_value.dropna(subset=['Value'])
    # Set the 'Time (Europe/Berlin)' column as the index
    df_value['Time (Europe/Berlin)'] = pd.to_datetime(df_value['Time (Europe/Berlin)'])
    # Round the time to the nearest second
    df_value['RoundedTime'] = df_value['Time (Europe/Berlin)'].dt.round('1s')

    df_new = pd.DataFrame({'Time': df_value['RoundedTime'].unique()})

    def process_binding_id(l):
        print(l)
        newvalues = []
        for k in df_value['RoundedTime'].unique():
            try:
                newvalues.append(
                    df_value[(df_value['RoundedTime'] == k) & (df_value['BindingId'] == l)]['Value'].values[0])
            except:
                newvalues.append(np.nan)
        return l, newvalues

    # Specify the number of parallel processes (adjust as needed)
    num_processes = -1  # Use all available processors

    results = Parallel(n_jobs=num_processes)(
        delayed(process_binding_id)(l) for l in df_bindings['BindingId'].unique()
    )

    for result in results:
        l, newvalues = result
        df_new[l] = newvalues

    if save:
        df_new.to_csv('Masterflex-KI/Converted_with_log/' + file[:-4] + 'csv', index=False)


def get_diaries(path='Masterflex-KI/Exporte', file='Seq-58.xlsx'):
    df_value = pd.read_excel(os.path.join(path,file), sheet_name='values')

    return {'File': file,
            'Values': df_value['Value'][df_value['BindingId'] == 'ba926976-de42-4acf-8b09-6db887c104b3'].values}

def remove_rows(df: pd.DataFrame):
    for cur_row in df.columns.values[2:]:
        # df[cur_row].isna.any()
        if df[cur_row].isna().any():
            df.drop(cur_row, axis=1, inplace=True)

    return df

def find_nans(save=False):

    files = ['Seq-38.xlsx',
    'Seq-58.xlsx',
    'Seq-61.xlsx',
    'Seq-70.xlsx',
    'Seq-76.xlsx',
    'Seq-78.xlsx']


    path='Masterflex-KI/Exporte'
    path2='Masterflex-KI/Converted'

    df_bindings = pd.read_excel(os.path.join(path, files[1]), sheet_name='bindings')

    # for filename in files[1:]:
    #     read_data_parallel(path=path, file=filename, save=True)
    df_ = pd.DataFrame({'BindingIds': df_bindings['BindingId'].values})
    names = []
    for k in df_['BindingIds']:
        names.append(df_bindings[df_bindings['BindingId'] == k]['Name'].values)
    df_['Name'] = names

    for file in files:
        df = pd.read_csv(os.path.join(path2, file[:-4] + 'csv'))
        cur_list = []
        for k in range(df_bindings['BindingId'].shape[0]):
            cur_list.append(df[df_bindings['BindingId'][k]].isna().all())
        df_[file] = cur_list
    if save:
        df_.to_excel(os.path.join('Masterflex-KI/Missing_values.xlsx'))
    return df_

def load_and_preprocess(path='Masterflex-KI/Converted_with_log', filename='Seq-38', series=1, use_diary=False):

    mapping = pd.read_excel('Masterflex-KI/Mapping.xlsx')
    features = pd.read_csv(os.path.join(path, filename + '.csv'), delimiter=',')
    start_features = pd.read_excel(os.path.join('Masterflex-KI/Exporte/', filename + '_Startwerte.xlsx'))
    columns_to_drop = []
    if use_diary:
        features['ba926976-de42-4acf-8b09-6db887c104b3'].fillna(0, inplace=True)
    for cur_column in features.columns[1:]:
        if pd.isna(features[cur_column][0]):
            cur_name = mapping['Name'][mapping['BindingId']==cur_column]
            if (pd.isna(start_features[cur_name.values[0]][0]) or
                    isinstance(start_features[cur_name.values[0]][0], str)):
                columns_to_drop.append(cur_column)
                # print(cur_name.values[0])
                # print(start_features[cur_name.values[0]][0])
            features.loc[0, cur_column] = start_features[cur_name.values[0]][0]


    features.drop(columns_to_drop, axis=1, inplace=True)
    # features = remove_rows(features)
    # features.fillna(0, inplace=True)
    features.replace(method='ffill', inplace=True)

    features['Time'] = pd.to_datetime(features['Time'])

    # Calculate the time elapsed in seconds from the first value
    features['seconds'] = (features['Time'] - features['Time'].iloc[0]).dt.total_seconds().astype(int)

    # features['series'] = np.full(len(features['Time']), series)
    return features, columns_to_drop

def filter_features(seq):
    import matplotlib.pyplot as plt
    path = '/Users/e230-admin/anomaly_detection_process_data/Masterflex-KI/all_sequences'
    dirs = os.listdir(path)
    for cur_seq in :
        print(cur_seq)
        seq = pd.read_csv(os.path.join(path, cur_seq, 'features.csv'))
        # plt.plot(seq[seq.columns[4]])
        # plt.show()
        # x=0
        # while x==0:
        for index, cur_values in seq.iterrows():
            if cur_values.iloc[14] < 10 or cur_values.iloc[3] < 450 or cur_values.iloc[25] < 5:
                print('yes')
                x=1
                break
                # else:
                #     print('')
                # seq.drop(index, inplace=True)



def main():
    files = ['Seq-38.xlsx',
             'Seq-58.xlsx',
             'Seq-61.xlsx',
             'Seq-70.xlsx',
             'Seq-76.xlsx',
             'Seq-78.xlsx',
             'Seq-83.xlsx',
             'Seq-85.xlsx',
             'Seq-86.xlsx',
             'Seq-87.xlsx',
             'Seq-88.xlsx',
             'Seq-92.xlsx']
    mydict = {'File': [],
              'Values': []}
    for file in files:
        # read_data_parallel('/Users/e230-admin/anomaly_detection_process_data/Masterflex-KI/Exporte', file, use_diary=True)
        newdict = get_diaries('/Users/e230-admin/anomaly_detection_process_data/Masterflex-KI/Exporte', file)
        mydict['File'].append(newdict['File'])
        mydict['Values'].append(newdict['Values'])

#
# if __name__ == "__main__":
#     main()
