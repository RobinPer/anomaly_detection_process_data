import numpy as np
from sklearn.ensemble import IsolationForest
import matplotlib.pyplot as plt
import pandas as pd
from utils import *
from preprocess import load_and_preprocess  # Assuming preprocess.py exists and contains load_and_preprocess function
import shap

from sklearn.preprocessing import StandardScaler
def explain_samples_prediction(sample: pd.DataFrame, isolation_forest, title):
    predictions = isolation_forest.predict(sample[sample.columns[1:]])
    sample['anomaly_label'] = predictions
    anomalies = sample[sample.anomaly_label == -1].head()
    explainer = shap.Explainer(isolation_forest.predict, anomalies[anomalies.columns[1:-1]])
    shap_values = explainer(anomalies[anomalies.columns[1:-1]])
    shap.initjs()
    shap.plots.force(shap_values[1])

def run_isolation_forest(df_all: pd.DataFrame, contamination=0.1):
    features = df_all[df_all.columns[1:]]

    # Create a StandardScaler instance
    scaler = StandardScaler()

    # Perform feature scaling
    scaled_features = scaler.fit_transform(features)

    # Create an Isolation Forest model
    isolation_forest = IsolationForest(n_estimators=400, max_samples='auto',
                                       contamination=contamination, max_features=13,
                                       bootstrap=False, n_jobs=-1, random_state=42)

    # Fit the model to the scaled features
    isolation_forest.fit(scaled_features)

    # Return the trained isolation forest model and the scaler
    return isolation_forest, scaler

def plot_single_seq(features, classifier, scaler, figtitle, save=False, Bindings=None):
    mapping = pd.read_excel('Masterflex-KI/Mapping.xlsx')

    # Perform feature scaling using the provided scaler
    scaled_features = scaler.transform(features[features.columns[1:]])

    predictions = classifier.predict(scaled_features)
    features['anomaly_label'] = predictions

    new_feat = features.copy()
    for cur_col in new_feat.columns[1:-2]:
        print(cur_col)
        # Get the name from the mapping DataFrame based on the BindingId
        name = mapping.loc[mapping['BindingId'] == cur_col, 'Name'].values[0]
        print(name)
        # Replace the column name in features DataFrame
        new_feat.rename(columns={cur_col: name}, inplace=True)
        new_feat['Time'] = features['Time'].dt.tz_localize(None)
        new_feat.to_excel('Masterflex-KI/Results_isolation/' + figtitle + '.xlsx')

    std_norm = []
    df_col = []
    for cur_column in features.columns[1:-2]:
        cur_std = features[cur_column].std()
        if not cur_std == 0:
            std_norm.append(features[cur_column].std() / features[cur_column].mean())
            df_col.append(cur_column)
        else:
            std_norm.append(0)
            df_col.append(cur_column)


    # Sort std_norm and df_col based on the standard deviation values in descending order
    sorted_indices = sorted(range(len(std_norm)), key=lambda i: std_norm[i], reverse=True)
    highest_std_indices = sorted_indices[:6]  # Get indices of the six highest standard deviations

    # Get the six highest standard deviations and corresponding column names
    six_highest_std = [std_norm[i] for i in highest_std_indices]
    six_highest_col_names = [df_col[i] for i in highest_std_indices]

    # Create subplots for the six highest standard deviation features
    fig, axs = plt.subplots(2, 3, figsize=(15, 10))
    for ax, cur_name in zip(axs.flat, six_highest_col_names):
        print(cur_name)
        ax.plot(features['seconds'],
                features[cur_name])
        # Highlight outliers in red
        it = 0
        for index, row in features.iterrows():
            if row['anomaly_label'] == -1:
                it += 1
                ax.plot([row['seconds']], row[cur_name], 'ro', color='red')  #

        print(it)

        ax.set_xlabel('Seconds')
        ax.set_ylabel(mapping[mapping['BindingId'] == cur_name]['Name'].values[0])
        ax.set_title('Feature: ' + cur_name, fontsize=9)

    # Adjust layout
    # plt.tight_layout()

    fig.suptitle(figtitle, fontsize=15)

    # Show the subplots
    if save:
        plt.savefig('Masterflex-KI/Results_isolation_08/' + figtitle + '.png')
    plt.show()

def plot_most_contributing_features(features, classifier, scaler, figtitle, save=False, Bindings=None, timepoint=None):
    mapping = pd.read_excel('Masterflex-KI/Mapping.xlsx')

    # Perform feature scaling using the provided scaler
    scaled_features = scaler.transform(features[features.columns[1:]])

    predictions = classifier.predict(scaled_features)
    features['anomaly_label'] = predictions

    new_feat = features.copy()
    for cur_col in new_feat.columns[1:-2]:
        print(cur_col)
        # Get the name from the mapping DataFrame based on the BindingId
        name = mapping.loc[mapping['BindingId'] == cur_col, 'Name'].values[0]
        print(name)
        # Replace the column name in features DataFrame
        new_feat.rename(columns={cur_col: name}, inplace=True)
        new_feat['Time'] = features['Time'].dt.tz_localize(None)
        new_feat.to_excel('Masterflex-KI/Results_isolation/' + figtitle + '.xlsx')

    std_norm = []
    df_col = []
    for cur_column in features.columns[1:-2]:
        cur_std = features[cur_column].std()
        if not cur_std == 0:
            std_norm.append(features[cur_column].std() / features[cur_column].mean())
            df_col.append(cur_column)
        else:
            std_norm.append(0)
            df_col.append(cur_column)


    # Sort std_norm and df_col based on the standard deviation values in descending order
    sorted_indices = sorted(range(len(std_norm)), key=lambda i: std_norm[i], reverse=True)
    highest_std_indices = sorted_indices[:6]  # Get indices of the six highest standard deviations

    # Get the six highest standard deviations and corresponding column names
    six_highest_std = [std_norm[i] for i in highest_std_indices]
    six_highest_col_names = [df_col[i] for i in highest_std_indices]

    # Create subplots for the six highest standard deviation features
    fig, axs = plt.subplots(2, 3, figsize=(15, 10))
    for ax, cur_name in zip(axs.flat, six_highest_col_names):
        print(cur_name)
        ax.plot(features['seconds'],
                features[cur_name])
        # Highlight outliers in red
        it = 0
        for index, row in features.iterrows():
            if row['anomaly_label'] == -1:
                it += 1
                ax.plot([row['seconds']], row[cur_name], 'ro', color='red')  #

        print(it)

        ax.set_xlabel('Seconds')
        ax.set_ylabel(mapping[mapping['BindingId'] == cur_name]['Name'].values[0])
        ax.set_title('Feature: ' + cur_name, fontsize=9)

    # Adjust layout
    # plt.tight_layout()

    fig.suptitle(figtitle, fontsize=15)

    # Show the subplots
    if save:
        plt.savefig('Masterflex-KI/Results_isolation_08/' + figtitle + '.png')
    plt.show()


def explain_anomaly(seq, scaler, isolation_forest, timepoint, plot=False):
    mapping = pd.read_excel('Masterflex-KI/Mapping.xlsx')
    scaled_features = scaler.transform(seq[seq.columns[1:]])
    scaled_features = pd.DataFrame(scaled_features, columns=seq.columns[1:])
    predictions = isolation_forest.predict(scaled_features)
    # seq['anomaly_label'] = predictions
    # anomalies = seq[seq.anomaly_label == -1]

    time_condition = (seq['seconds'] > timepoint - 2) & (seq['seconds'] < timepoint + 2)
    explain_sample = scaled_features[time_condition]

    explainer = shap.Explainer(isolation_forest)
    shap_values = explainer(explain_sample)
    if plot:
        shap.initjs()
        shap.plots.waterfall(shap_values[1], show=False)
        plt.tight_layout()
        plt.savefig('Masterflex-KI/test.png')  # Save the image before displaying it
        plt.show()  # Display the plot

    # shap_idx = np.argsort(shap_values[0].values)[:6]
    # important_ids = []
    # for cur_idx in shap_idx:
    #     print(mapping['Name'][mapping['BindingId']==seq.columns[cur_idx]])
    #     important_ids.append(seq.columns[cur_idx+1])
    return shap_values

def main():

    files = ['Seq-38',
             'Seq-58',
             'Seq-61',
             'Seq-70',
             'Seq-76',
             'Seq-78',
             'Seq-83',
             'Seq-85',
             'Seq-86',
             'Seq-87',
             'Seq-88',
             'Seq-92']

    files_test = ['Seq-70','Seq-86', 'Seq-38']


    df = get_data_array(files)

    df = rename_features(df)

    # train_idx = [idx for idx, file in enumerate(files) if file not in files_test]
    # df_train = [df[i] for i in train_idx]  # Filtering out dataframes using train_idx
    # df_train = pd.concat(df_train, axis=0)
    df_train = pd.concat(df, axis=0)


    # test_idx = [idx for idx, file in enumerate(files) if file in files_test]
    test_idx = [idx for idx, file in enumerate(files) if file in files]
    # Isolation Forest
    isolation_forest, scaler = run_isolation_forest(df_train, contamination=0.08)

    for k in test_idx:
        print(files[k])
        plot_single_seq(df[k].copy(), isolation_forest, scaler, figtitle=files[k], save=True, Bindings=None)

    explain_anomaly(seq = df[1].copy(), scaler=scaler, isolation_forest=isolation_forest, timepoint=300)



# if __name__ == "__main__":
#     main()

