import pandas as pd
import json
import math

# Load the Excel file
input_file = '/home/robin/anomaly_detection_process_data/Masterflex-KI/63/MO108401/features.xlsx'  # Replace with the actual path to your Excel file
input_file2 = '/home/robin/anomaly_detection_process_data/Masterflex-KI/63_new/MO108401/features.xlsx'  # Replace with the actual path to your Excel file
df = pd.read_excel(input_file)
df2 = pd.read_excel(input_file2)

# Define the output file
output_file = 'MO108401.txt'

# Open the output file in write mode
with open(output_file, 'w') as file:
    # Iterate over each row in the DataFrame
    for (index1, row1), (index2, row2) in zip(df.iterrows(), df2.iterrows()):
        # Write a formatted string to the file with values from both rows
        # Extract the timestamp (assuming it’s in the 'seconds' column)
        ms = int(row1['seconds']) if 'seconds' in df.columns else index1  # Use row index if 'seconds' is missing

        # Iterate over each column except 'seconds'
        for (col_name, value), (col_name2, value2) in zip(row1.items(), row2.items()):
            # Ignore 'seconds' column and NaN values
            if col_name == 'seconds' or (isinstance(value, float) and math.isnan(value)):
                continue

            # Construct dictionary format
            data_dict = {
                "ms": ms,
                "start event": col_name2 ,
                "objectkey": col_name ,
                "value": value
            }


            # Convert dictionary to JSON and write to the file
            json_line = json.dumps(data_dict)
            file.write(json_line + "\n")

print(f"Data has been written to {output_file}")