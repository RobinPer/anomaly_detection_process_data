import pandas as pd
from pyts.preprocessing import StandardScaler
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import LabelEncoder
from sklearn.metrics import mean_squared_error, mean_absolute_error
import numpy as np
import matplotlib.pyplot as plt
import random
from xgboost import XGBRegressor


def create_dataset(series, x_train, x_label, y):
    all_df_train = pd.DataFrame(columns=x_train + x_label)
    all_df_y = pd.DataFrame(columns=y)

    for cur_series in series:
        print(f"Processing series: {cur_series}")
        cur_df_train, cur_df_y = preprocess_timeseries(cur_series, x_label, x_train, y)
        all_df_train = pd.concat([all_df_train, cur_df_train], ignore_index=True)
        all_df_y = pd.concat([all_df_y, cur_df_y], ignore_index=True)

    return all_df_train, all_df_y


def preprocess_timeseries(series: str, x_train, x_label, y):
    df = pd.read_csv(f'Masterflex-KI/new_sequences/{series}/features.csv', low_memory=False)
    df_train = df.copy()[x_train]
    df_label = df.copy()[x_label].astype('string')
    df_train[x_label] = df_label
    df_y = df.copy()[y]
    df_train.ffill(inplace=True)
    df_y.ffill(inplace=True)

    condition_col = 'f022cf4f-dc76-4196-a41d-654ed24b2aa1'
    condition_value = 'Bearbeitung'
    if condition_col in df.columns:
        first_occurrence = df[df[condition_col] == condition_value].index.min()
        if pd.notna(first_occurrence):
            df = df.loc[first_occurrence:]
            df['seconds'] = df['seconds'] - df['seconds'].iloc[0]

    return df_train, df_y


def create_features(df, features, lags=[1, 5, 10], rolling_windows=[5, 10]):
    df = df.copy()
    for feature in features:
        for lag in lags:
            df[f'{feature}_lag_{lag}'] = df[feature].shift(lag)
        for window in rolling_windows:
            df[f'{feature}_roll_mean_{window}'] = df[feature].rolling(window).mean()
            df[f'{feature}_roll_std_{window}'] = df[feature].rolling(window).std()
    df.dropna(inplace=True)
    return df


def batch_generator(sequence_ids, batch_size=5):
    """Yields batches of sequence IDs."""
    for i in range(0, len(sequence_ids), batch_size):
        yield sequence_ids[i:i + batch_size]


def plot_pred(y_pred, y_test, series='Name'):
    fig, axs = plt.subplots(2, 2, figsize=(14, 10), sharex=False, sharey=False)
    labels = ['Schlauch_Außendurchmesser_Tal', 'Schlauch_Steigung_A',
              'Schlauch_Außendurchmesser', 'Schlauch_Steigung_B']

    for i, ax in enumerate(axs.flat):
        ax.plot(y_pred[:, i], label='Predicted Values', color='blue', linestyle='-', marker='o')
        ax.plot(y_test[:, i], label='True Values', color='orange', linestyle='--', marker='x')
        ax.set_title(labels[i])
        ax.set_xlabel('Time Step')
        ax.set_ylabel('Value')
        ax.grid(True)
        ax.legend(loc='best')

    fig.suptitle(f'{series}: MAE: {mean_absolute_error(y_pred, y_test):.2f}', fontsize=16)
    plt.tight_layout()
    plt.savefig(f'{series}.png')
    plt.show()


def custom_label_transform(label_encoder, labels):
    try:
        return label_encoder.transform(labels)
    except ValueError as e:
        print(f"Warning: {e}")
        unique_labels = label_encoder.classes_
        return labels.apply(
            lambda x: label_encoder.transform([x])[0] if x in unique_labels else unique_labels.max() + 1)


def main():
    x_train = ['0dbdbfbe-69a3-4ba6-b873-8893bca976a7', '5e3053f8-6d41-45ce-94e9-11e069f775a5',
               'e72186fc-d581-4368-917d-92ebd8124e5c', '2a4433b9-0f99-4cc2-9669-0b17eeaa62ab',
               '9265f861-e03e-4697-a132-ebfd93004827', '6b0c06fb-c6c8-49bc-a422-5b1ba2f7e4bc',
               'cf7314c0-c20c-4c53-b48f-debf29c87b43', '59997204-2b97-478b-9120-d1b3d784ad4d',
               '2acf6ab4-7288-4750-8c75-e04169ffc51e', '09545925-7c3e-4619-afe8-9b8ef5b01f24',
               '33d2504f-7719-43f0-9c1f-9a5dae89bc24', 'a36d9a05-6c14-4abf-aae9-4f9d09237a4d',
               '1310aedc-bcd7-429b-8aa9-2ed81d7533df', '5f22224c-8b0f-4985-bbf9-f5b01fda8d6b',
               'b16f6189-3804-4463-96fb-4bd444d0c276', '6289cd82-4bb2-4488-8975-4d2729bdfa9d',
               'bbb36936-1a05-406b-9178-2b790ca07a5d', '777f6918-12a2-4629-be75-5a669872f125',
               '50bfd169-8434-48b2-bf28-0a9665efdd84', 'f61fe432-3704-49cf-9559-ece9a1d45c5a',
               'eda51693-36df-4c68-8532-55bb21990351', '852d5936-7606-4bec-bbaf-9e92fa8b6145',
               '4d78ad32-b33b-4022-b6bc-4ba11e4544ef', '348b29c9-4a41-4efe-938f-3b54c2297c9f',
               '5101caad-35df-4d7f-8bce-80fefa4df866', '02ce84e5-d56f-40fd-8025-4dd85f5e2924',
               '45c84756-6a09-4040-80c6-15993de05bd4', '1115a47d-f41b-43e9-9804-3fa2f809b54a',
               'ed42f03f-09f6-4906-888e-10afd7871409', '30babb2b-84d1-4d7e-860f-2c1eae9ae177',
               'seconds']

    x_label = ['b0a51b71-7ba0-49a4-b2a2-13984d8c6616']

    y = ['c3677f59-6137-4e8f-97f3-077ed6a62f2a', 'b37fd9cb-d968-4004-8afc-80a3522048a5',
         '870e0de0-2235-43bf-b309-101e6dfad86f', 'd08e8aba-87ad-47c6-b373-de74d73a351e']


    sequence_ids = ['MO108331', 'MO108332', 'MO108333', 'MO108401', 'MO108407', 'MO108454', 'MO108534', 'MO108536',
                    'MO108726', 'MO108902', 'MO108980', 'MO109068', 'MO109073', 'MO109077', 'MO109151', 'MO109161',
                    'MO109162', 'MO109163', 'MO109240', 'MO109241', 'MO109301', 'MO109302', 'MO109345', 'MO109407',
                    'MO109409', 'MO109515', 'MO109528', 'MO109529', 'MO109586', 'MO109601', 'MO109745', 'MO109758',
                    'MO109759', 'MO109830', 'MO109914', 'MO111364', 'MO111365', 'MO111450', 'MO111531', 'MO111591',
                    'MO111616', 'MO111617', 'MO111618', 'MO111624', 'MO111866', 'MO111867', 'MO111915', 'MO112101',
                    'MO112102', 'MO112176', 'MO112254']

    all_df_train, all_df_y = create_dataset(sequence_ids, x_train, x_label, y)

    # Creating lagged features
    all_df_train = create_features(all_df_train, x_train)

    # Standardizing features
    scaler = StandardScaler()
    all_df_train = scaler.fit_transform(all_df_train)

    # Splitting dataset into training and testing
    X_train, X_test, y_train, y_test = train_test_split(all_df_train, all_df_y, test_size=0.2, random_state=42)

    # Define the model
    model = XGBRegressor(objective='reg:squarederror', n_estimators=100)

    # Training in batches
    for batch in batch_generator(X_train, batch_size=5):
        model.fit(batch, y_train.loc[batch.index])

    # Predicting in batches
    y_pred = []
    for batch in batch_generator(X_test, batch_size=5):
        preds = model.predict(batch)
        y_pred.extend(preds)

    y_pred = np.array(y_pred).reshape(-1, len(y))

    # Plot predictions
    plot_pred(y_pred, y_test.to_numpy(), series='Batch Processing Example')

#
# if __name__ == "__main__":
#     main()
