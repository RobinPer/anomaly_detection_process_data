import os.path
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
from sklearn.cluster import KMeans
from sklearn.manifold import TSNE
from sklearn.preprocessing import StandardScaler
from preprocess import load_and_preprocess
class KMeansTrainer:
    def __init__(self, 
                 path='data', 
                 files='seq-13'):
        self.path = path
        self.files = files

    def get_data(self,files):
        df = []
        droppinglist = []
        i = 1
        order = False
        for filename in files:
            print(filename)
            # df = pd.concat(df, load_and_preprocess(filename=filename))
            cur_df, cur_drop = load_and_preprocess(filename=filename)
            if order == False:
                all_order = cur_df.columns
                order = True
            cur_df = cur_df.reindex(columns=all_order)
            df.append(cur_df)
            droppinglist.append(cur_drop)
            i = i + 1

        # df_all = [df[i] for i in [0, 3, 4, 5, 6, 7, 8, 9, 10, 11]]

        return df

    def find_optimal_clusters(self, data, max_clusters=10):
        scaler = StandardScaler()
        scaled_features = scaler.fit_transform(data[data.columns[2:]])

        inertias = []
        for k in range(1, max_clusters + 1):
            kmeans = KMeans(n_clusters=k, random_state=42)
            kmeans.fit(scaled_features)
            inertias.append(kmeans.inertia_)

        plt.plot(range(1, max_clusters + 1), inertias, marker='o')
        plt.xlabel('Number of Clusters (k)')
        plt.ylabel('Inertia')
        plt.title('Elbow Method for Optimal k')
        plt.show()

    def run_kmeans_with_outlier_detection(self, features, optimal_clusters, outlier_threshold=4.0):
        scaler = StandardScaler()
        scaled_features = scaler.fit_transform(features[features.columns[1:]])

        kmeans = KMeans(n_clusters=optimal_clusters, random_state=42)
        features['cluster'] = kmeans.fit_predict(scaled_features)

        distances = np.min(kmeans.transform(scaled_features), axis=1)

        features['distance'] = distances
        features['is_outlier'] = distances > 5

        tsne = TSNE(n_components=3, random_state=42)
        tsne_result = tsne.fit_transform(scaled_features)

        features['tsne_1'] = tsne_result[:, 0]
        features['tsne_2'] = tsne_result[:, 1]
        features['tsne_3'] = tsne_result[:, 2]

        fig = plt.figure(figsize=(10, 8))
        ax = fig.add_subplot(111, projection='3d')

        normal_points = features[features['is_outlier'] == False]
        scatter_normal = ax.scatter(normal_points['tsne_1'], normal_points['tsne_2'], normal_points['tsne_3'],
                                    c=normal_points['cluster'], cmap='viridis', label='Normal Points')

        outliers = features[features['is_outlier'] == True]
        scatter_outliers = ax.scatter(outliers['tsne_1'], outliers['tsne_2'], outliers['tsne_3'],
                                      c='red', marker='x', label='Outliers')

        ax.set_xlabel('t-SNE Dimension 1')
        ax.set_ylabel('t-SNE Dimension 2')
        ax.set_zlabel('t-SNE Dimension 3')
        ax.set_title('K-Means Clustering and t-SNE Visualization with Outlier Detection in 3D')

        legend_normal = ax.legend(*scatter_normal.legend_elements(), title='Clusters')
        ax.add_artist(legend_normal)
        ax.legend(*scatter_outliers.legend_elements(), title='Clusters')

        plt.show()

        return kmeans, scaler

    def plot_single_seq(self, features, classifier, scaler, thresh, figtitle, save=False):
        mapping = pd.read_excel('Masterflex-KI/Mapping.xlsx')
        scaled_features = scaler.transform(features[features.columns[1:]])

        features['cluster'] = classifier.predict(scaled_features)
        distances = np.min(classifier.transform(scaled_features), axis=1)
        features['distance'] = distances
        features['is_outlier'] = distances > thresh

        if save:
            new_feat = features.copy()
            for cur_col in new_feat.columns[1:-4]:
                name = mapping.loc[mapping['BindingId'] == cur_col, 'Name'].values[0]
                new_feat.rename(columns={cur_col: name}, inplace=True)
            new_feat['Time'] = features['Time'].dt.tz_localize(None)
            new_feat.to_excel('Masterflex-KI/Results_kmeans/' + figtitle + '.xlsx')

        std_norm = []
        df_col = []
        for cur_column in features.columns[1:-4]:
            cur_std = features[cur_column].std()
            if cur_std != 0:
                std_norm.append(features[cur_column].std() / features[cur_column].mean())
                df_col.append(cur_column)
            else:
                std_norm.append(0)
                df_col.append(cur_column)

        sorted_indices = sorted(range(len(std_norm)), key=lambda i: std_norm[i], reverse=True)
        highest_std_indices = sorted_indices[:6]

        six_highest_std = [std_norm[i] for i in highest_std_indices]
        six_highest_col_names = [df_col[i] for i in highest_std_indices]

        fig, axs = plt.subplots(2, 3, figsize=(15, 10))
        for ax, cur_name in zip(axs.flat, six_highest_col_names):
            ax.plot(features['seconds'], features[cur_name])
            it = 0
            for index, row in features.iterrows():
                if row['is_outlier']:
                    it += 1
                    ax.plot([row['seconds']], row[cur_name], 'ro', color='red')
            ax.set_xlabel('Seconds')
            ax.set_ylabel(mapping[mapping['BindingId'] == cur_name]['Name'].values[0])
            ax.set_title('Feature: ' + cur_name, fontsize=9)

        fig.suptitle(figtitle, fontsize=15)

        if save:
            plt.savefig('Masterflex-KI/Results_kmeans/' + figtitle + '.png')
        plt.show()

    def train_kmeans(self):
        df = self.get_data_array(self.files)
        df_all = pd.concat(df, axis=0)

        self.find_optimal_clusters(df_all)
        optimal_clusters = 4
        kmeans, scaler = self.run_kmeans_with_outlier_detection(df_all, optimal_clusters, outlier_threshold=4)

        for k in range(len(df)):
            print(self.files[k])
            self.plot_single_seq(df[k].copy(), kmeans, scaler, thresh=5, figtitle=self.files[k], save=True)


def main():
    files = ['Seq-38']  # Add other files here if needed
    trainer = KMeansTrainer()
    trainer.train_kmeans(files)