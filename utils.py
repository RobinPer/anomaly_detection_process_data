import pandas as pd
import matplotlib.pyplot as plt
from sklearn.metrics import mean_squared_error, mean_absolute_error
import numpy as np
def get_data_array(files):
    df = []
    droppinglist = []
    i = 1
    order = False
    for filename in files:
        print(filename)
        # df = pd.concat(df, load_and_preprocess(filename=filename))
        cur_df, cur_drop = load_and_preprocess(filename=filename)
        if order == False:
            all_order = cur_df.columns
            order = True
        cur_df = cur_df.reindex(columns=all_order)
        df.append(cur_df)
        droppinglist.append(cur_drop)
        i = i + 1

    # df_all = [df[i] for i in [0, 3, 4, 5, 6, 7, 8, 9, 10, 11]]

    return df

def rename_features(df):
    mapping = pd.read_excel('Masterflex-KI/Mapping.xlsx')
    features_names = df[0].columns
    real_names = [features_names[0]]
    for cur_column in features_names.values[1:-1]:

        cur_name = mapping['Name'][mapping['BindingId']==cur_column].values[0]
        print(cur_name[:10])
        print(cur_name)
        real_names.append(cur_name)
    real_names.append(features_names[-1])
    for curdf in range(len(df)):
        df[curdf].columns = real_names

    return df

def create_dataset(series, x_train, x_label, y, rolling=False):
    # Initialize empty DataFrames with the proper column names
    all_df_train = pd.DataFrame(columns=x_train + x_label)  # Columns are the concatenation of x_train and x_label
    all_df_y = pd.DataFrame(columns=y)  # y is the target column name

    for cur_series in series:
        print(cur_series)

        # Assuming preprocess_timeseries returns two DataFrames
        cur_df_train, cur_df_y = preprocess_timeseries(cur_series, x_train, x_label, y, rolling=rolling)

        # Concatenate the current DataFrames with the accumulated DataFrames
        all_df_train = pd.concat([all_df_train, cur_df_train], ignore_index=True)
        all_df_y = pd.concat([all_df_y, cur_df_y], ignore_index=True)

    return all_df_train, all_df_y

def preprocess_timeseries(series: str, x_train, x_label, y, rolling=False):

    df = pd.read_csv(f'Masterflex-KI/new_sequences/{series}/features.csv', low_memory=False)


    # Copy x_train columns to df_train
    df_train = df.copy()[x_train]
    print(df['d85d28ca-fb7c-4ec3-951a-7972e5b62925'][0])
    print(df[y[0]].mean())
    # Copy x_label columns and convert to string
    df_label = df.copy()[x_label].astype('string')

    # Add the converted df_label back to df_train
    df_train[x_label] = df_label
    df_y = df.copy()[y]

    # Forward fill any NA values in both df_train and df_y
    df_train.ffill(inplace=True)
    df_y.ffill(inplace=True)
    # Remove rows before the first occurrence of a specific condition in a column
    condition_col = 'f022cf4f-dc76-4196-a41d-654ed24b2aa1'  # Column ID
    condition_value = 'Bearbeitung'  # Target value to find in this column

    if condition_col in df.columns:
        first_occurrence = df[df[condition_col] == condition_value].index.min()
        if pd.notna(first_occurrence):
            df_train = df_train.loc[first_occurrence:].copy()  # Keep only rows from the first occurrence onward
            df_y = df_y.loc[first_occurrence:].copy()
            # Reset 'seconds' column to start at 0
            if 'seconds' in df_train.columns:
                df_train['seconds'] = pd.to_numeric(df_train['seconds'], errors='coerce')
                df_train['seconds'] = df_train['seconds'] - df_train['seconds'].iloc[0]

    print(len(df_train))
    print(len(df_y))
    #
    if rolling:
        df_train[x_train] = df_train[x_train].rolling(window=60, min_periods=1).mean()
        df_y = df_y.rolling(window=60, min_periods=1).mean()

    return df_train, df_y

def custom_label_transform(label_encoder, labels):
    # Use a try-except block to handle unseen labels
    try:
        return label_encoder.transform(labels)
    except ValueError as e:
        print(f"Warning: {e}")
        unique_labels = label_encoder.classes_
        print(f"New label is {unique_labels.max() + 1}")
        # Map unseen labels to a special value (e.g., -1)
        return labels.apply(
            lambda x: label_encoder.transform([x])[0] if x in unique_labels else unique_labels.max() + 1)

def plot_pred(y_pred, y_test, series='Name'):
    # Create a 2x2 grid of subplots
    fig, axs = plt.subplots(2, 2, figsize=(14, 10), sharex=False, sharey=False)

    # Define labels for each subplot
    labels = ['Schlauch_Außendurchmesser_Tal', 'Schlauch_Steigung_A',
              'Schlauch_Außendurchmesser', 'Schlauch_Steigung_B']

    # Iterate over each subplot and corresponding target
    for i, ax in enumerate(axs.flat):
        ax.plot(y_test[:, i], label='True Values', color='orange', linestyle='--')
        ax.plot(y_pred[:, i], label='Predicted Values', color='blue', linestyle='-')

        # Calculate the Mean Absolute Error for the current subplot
        mae = mean_absolute_error(y_test[:, i], y_pred[:, i])

        # Set title with MAE for each subplot
        ax.set_title(f'{labels[i]}: MAE: {mae:.2f}')
        ax.set_xlabel('Time Step')
        ax.set_ylabel('Value')
        ax.grid(True)
        ax.legend(loc='best')

        # Determine y-axis range
        min_val = min(min(y_pred[:, i]), min(y_test[:, i]))
        max_val = max(max(y_pred[:, i]), max(y_test[:, i]))

        # Ensure y-axis range is at least 6
        if max_val - min_val < 6:
            mid_val = (min_val + max_val) / 2
            min_val, max_val = np.floor(mid_val - 3), np.ceil(mid_val + 3)

        # Set the y-axis ticks to 1-step intervals within adjusted range, using only integers
        yticks = np.arange(int(min_val), int(max_val) + 1, 1)
        ax.set_yticks(yticks)
        ax.set_ylim(min_val, max_val)

    # Add main title for the entire figure with overall MAE
    fig.suptitle(f'{series}: Overall MAE: {mean_absolute_error(y_test, y_pred):.2f}', fontsize=16)

    plt.tight_layout()
    plt.savefig(f'Masterflex-KI/Images/{series}.png')
    # Show the plots
    plt.show()

def plot_gt(data, series='Name'):
    # Create a 2x2 grid of subplots
    fig, axs = plt.subplots(2, 2, figsize=(14, 10), sharex=False, sharey=False)

    # Define labels for each subplot
    labels = ['Schlauch_Außendurchmesser_Tal', 'Schlauch_Steigung_A',
              'Schlauch_Außendurchmesser', 'Schlauch_Steigung_B']

    # Iterate over each subplot and corresponding target
    for i, ax in enumerate(axs.flat):
        ax.plot(data[:, i], label='True Values', color='orange', linestyle='--')
        # ax.plot(y_pred[:, i], label='Predicted Values', color='blue', linestyle='-', marker='o')


        # Set title with MAE for each subplot

        ax.set_title(f'{labels[i]}')
        ax.set_xlabel('Time Step')
        ax.set_ylabel('Value')
        ax.grid(True)
        ax.legend(loc='best')

        # Determine y-axis range
        min_val = min(data[:, i])
        max_val = max(data[:, i])

        # Ensure y-axis range is at least 6
        if max_val - min_val < 6:
            mid_val = (min_val + max_val) / 2
            min_val, max_val = np.floor(mid_val - 3), np.ceil(mid_val + 3)

        # Set the y-axis ticks to 1-step intervals within adjusted range, using only integers
        yticks = np.arange(int(min_val), int(max_val) + 1, 1)
        ax.set_yticks(yticks)
        ax.set_ylim(min_val, max_val)

    # Add main title for the entire figure with overall MAE
    fig.suptitle(f'{series}')

    plt.tight_layout()
    plt.savefig(f'Masterflex-KI/Images/{series}_gt.png')
    # Show the plots
    plt.show()



def create_features(df, features, lags=[1, 5, 10], rolling_windows=[5, 10]):
    df = df.copy()
    for feature in features:
        for lag in lags:
            df[f'{feature}_lag_{lag}'] = df[feature].shift(lag)  # Lagged features
        for window in rolling_windows:
            df[f'{feature}_roll_mean_{window}'] = df[feature].rolling(window).mean()  # Rolling mean
            df[f'{feature}_roll_std_{window}'] = df[feature].rolling(window).std()  # Rolling std
    df.dropna(inplace=True)  # Drop rows with NaN created by lag/rolling operations
    return df


def create_temporal_dataset(data, labels, sequence_length=5):
    """Prepare the dataset to include temporal features."""
    X, y = [], []
    for i in range(sequence_length, len(data)):
        X.append(data[i - sequence_length:i])  # Previous 4 seconds plus current second
        y.append(labels[i])  # Target variable for the current second

    return np.array(X), np.array(y)

