import torch
import torch.nn as nn

from torch.nn.utils.rnn import pack_padded_sequence, pad_packed_sequence
class SensorLSTM(nn.Module):
    def __init__(self, input_size, hidden_size, num_layers, output_size):
        super(SensorLSTM, self).__init__()
        self.lstm = nn.LSTM(input_size, hidden_size, num_layers, batch_first=True, dropout=0.2)
        self.fc = nn.Linear(hidden_size, output_size)

    def forward(self, x, lengths):
        # Pack the padded input sequences
        packed_x = pack_padded_sequence(x, lengths, batch_first=True, enforce_sorted=False)

        # Forward pass through LSTM
        packed_out, (hn, cn) = self.lstm(packed_x)

        # Unpack the sequences
        out, _ = pad_packed_sequence(packed_out, batch_first=True)

        # Apply the fully connected layer to each time step
        out = self.fc(out)

        return out
