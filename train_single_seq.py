import pandas as pd
from pyts.preprocessing import StandardScaler
from sklearn.model_selection import train_test_split
from sklearn.ensemble import RandomForestRegressor
from xgboost import XGBRegressor
from sklearn.svm import SVR
from sklearn.preprocessing import StandardScaler, LabelEncoder
from sklearn.ensemble import GradientBoostingClassifier as GBC
from sklearn.metrics import mean_squared_error, mean_absolute_error
import numpy as np
import matplotlib.pyplot as plt
import random
from sklearn.model_selection import GridSearchCV  # Import GridSearchCV
from utils import *

def main():
    scaling = True

    # Define the parameter grid for XGBRegressor

    # Train Model on these features
    x_train = ['0dbdbfbe-69a3-4ba6-b873-8893bca976a7', '5e3053f8-6d41-45ce-94e9-11e069f775a5',
               'e72186fc-d581-4368-917d-92ebd8124e5c', '2a4433b9-0f99-4cc2-9669-0b17eeaa62ab',
               '9265f861-e03e-4697-a132-ebfd93004827', '6b0c06fb-c6c8-49bc-a422-5b1ba2f7e4bc',
               'cf7314c0-c20c-4c53-b48f-debf29c87b43', '59997204-2b97-478b-9120-d1b3d784ad4d',
               '2acf6ab4-7288-4750-8c75-e04169ffc51e', '09545925-7c3e-4619-afe8-9b8ef5b01f24',
               '33d2504f-7719-43f0-9c1f-9a5dae89bc24', 'a36d9a05-6c14-4abf-aae9-4f9d09237a4d',
               '1310aedc-bcd7-429b-8aa9-2ed81d7533df', '5f22224c-8b0f-4985-bbf9-f5b01fda8d6b',
               'b16f6189-3804-4463-96fb-4bd444d0c276', '6289cd82-4bb2-4488-8975-4d2729bdfa9d',
               'bbb36936-1a05-406b-9178-2b790ca07a5d', '777f6918-12a2-4629-be75-5a669872f125',
               '50bfd169-8434-48b2-bf28-0a9665efdd84', 'f61fe432-3704-49cf-9559-ece9a1d45c5a',
               'eda51693-36df-4c68-8532-55bb21990351', '852d5936-7606-4bec-bbaf-9e92fa8b6145',
               '4d78ad32-b33b-4022-b6bc-4ba11e4544ef', '348b29c9-4a41-4efe-938f-3b54c2297c9f',
               '5101caad-35df-4d7f-8bce-80fefa4df866', '02ce84e5-d56f-40fd-8025-4dd85f5e2924',
               '45c84756-6a09-4040-80c6-15993de05bd4', '1115a47d-f41b-43e9-9804-3fa2f809b54a',
               'ed42f03f-09f6-4906-888e-10afd7871409', '30babb2b-84d1-4d7e-860f-2c1eae9ae177',
               'seconds']
    x_label = [
        'b0a51b71-7ba0-49a4-b2a2-13984d8c6616']  # , 'f022cf4f-dc76-4196-a41d-654ed24b2aa1', 'd85d28ca-fb7c-4ec3-951a-7972e5b62925']
    x_adaptable = ['5e3053f8-6d41-45ce-94e9-11e069f775a5', 'e72186fc-d581-4368-917d-92ebd8124e5c',
                   '2a4433b9-0f99-4cc2-9669-0b17eeaa62ab', '6b0c06fb-c6c8-49bc-a422-5b1ba2f7e4bc',
                   'cf7314c0-c20c-4c53-b48f-debf29c87b43', '59997204-2b97-478b-9120-d1b3d784ad4d',
                   '2acf6ab4-7288-4750-8c75-e04169ffc51e', '09545925-7c3e-4619-afe8-9b8ef5b01f24',
                   '33d2504f-7719-43f0-9c1f-9a5dae89bc24', '1310aedc-bcd7-429b-8aa9-2ed81d7533df',
                   'bbb36936-1a05-406b-9178-2b790ca07a5d', '777f6918-12a2-4629-be75-5a669872f125',
                   '50bfd169-8434-48b2-bf28-0a9665efdd84', 'eda51693-36df-4c68-8532-55bb21990351',
                   '4d78ad32-b33b-4022-b6bc-4ba11e4544ef', '348b29c9-4a41-4efe-938f-3b54c2297c9f',
                   '5101caad-35df-4d7f-8bce-80fefa4df866', '02ce84e5-d56f-40fd-8025-4dd85f5e2924',
                   '45c84756-6a09-4040-80c6-15993de05bd4', '1115a47d-f41b-43e9-9804-3fa2f809b54a',
                   'ed42f03f-09f6-4906-888e-10afd7871409']
    y = ['c3677f59-6137-4e8f-97f3-077ed6a62f2a', 'b37fd9cb-d968-4004-8afc-80a3522048a5',
         '870e0de0-2235-43bf-b309-101e6dfad86f', 'd08e8aba-87ad-47c6-b373-de74d73a351e']  # target outputs

    # All sequences
    # sequence_ids = ['MO112401', 'MO109301', 'MO111618', 'MO109174', 'MO111915', 'MO108058', 'MO109011', 'MO111624',
    #                 'MO112101', 'MO107772', 'MO109302', 'MO109161', 'MO109149', 'MO109068', 'MO109308', 'MO109914',
    #                 'MO109345', 'MO108912', 'MO109170', 'MO109089', 'MO109515', 'MO111365', 'MO109237', 'MO108801',
    #                 'MO109601', 'MO112102', 'MO109241', 'MO108980', 'MO111450', 'MO109759', 'MO109086', 'MO111616',
    #                 'MO107892', 'MO109169', 'MO108466', 'MO109586', 'MO111531', 'MO109409', 'MO109251', 'MO111867',
    #                 'MO108467', 'MO108799', 'MO109181', 'MO108792', 'MO109090', 'MO109745', 'MO108736', 'MO111630',
    #                 'MO109529', 'MO109091', 'MO108465', 'MO108794', 'MO111617', 'MO109805', 'MO111708', 'MO109171',
    #                 'MO109163', 'MO109830', 'MO108800', 'MO109407', 'MO111866', 'MO109758', 'MO108468', 'MO111591',
    #                 'MO109183', 'MO108384', 'MO109151', 'MO109077', 'MO109248', 'MO108795', 'MO109249', 'MO109176',
    #                 'MO111364', 'MO109425', 'MO108731', 'MO109087', 'MO109073', 'MO108874', 'MO112254', 'MO109162',
    #                 'MO109496', 'MO108668', 'MO109175', 'MO109424', 'MO109240', 'MO108544', 'MO109528', 'MO108737',
    #                 'MO112176']

    # Only 000063
    # sequence_ids = ['MO109151', 'MO108332', 'MO108333', 'MO108401', 'MO108407', 'MO108454', 'MO108534', 'MO108536',
    #                 'MO108726', 'MO108902', 'MO108980', 'MO109068', 'MO109073', 'MO109077', 'MO109151', 'MO109161',
    #                 'MO109162', 'MO109163', 'MO109240', 'MO109241', 'MO109301', 'MO109302', 'MO109345', 'MO109407',
    #                 'MO109409', 'MO109515', 'MO109528', 'MO109529', 'MO109586', 'MO109601', 'MO109745', 'MO109758',
    #                 'MO109759', 'MO109830', 'MO109914', 'MO111364', 'MO111365', 'MO111450', 'MO111591',
    #                 'MO111616', 'MO111617', 'MO111618', 'MO111624', 'MO111866', 'MO111867', 'MO111915', 'MO112101',
    #                 'MO112102', 'MO112176', 'MO112254']

    # # Only 000063:51
    sequence_ids = ['MO108332', 'MO108401', 'MO108534', 'MO108902', 'MO108980', 'MO109073', 'MO109161', 'MO109162',
                    'MO109301', 'MO109345', 'MO109407', 'MO109528', 'MO109529', 'MO109601', 'MO109758', 'MO111364',
                    'MO111365', 'MO111450', 'MO111616', 'MO111617', 'MO111618', 'MO111866', 'MO111915', 'MO112101',
                    'MO112176']

    for seq in sequence_ids:
        data, data_y = create_dataset([seq],
                                      x_train,
                                      x_label,
                                      y,
                                      rolling=True)

        # # Convert all string columns to categorical or numerical values
        # for col in data.select_dtypes(include=['object', 'string']).columns:
        #     # Convert to categorical codes if the column is categorical
        #     data[col] = data[col].astype('category').cat.codes

        label_encoder = LabelEncoder()
        data[x_label[0]] = label_encoder.fit_transform(data[x_label[0]])
        # data_test, data_test_y = create_dataset(['MO112102', 'MO112176', 'MO112254', 'MO112401'],
        data = data.values
        data_y = data_y.values

        # plot_gt(data_y, seq+'rolling')

        data, data_y = create_temporal_dataset(data, data_y, sequence_length=10)

        n_samples, n_time_steps, n_features = data.shape
        data = data.reshape(n_samples, -1)  # Flatten to 2D for scaling

        data_train = data[: int(0.75 * len(data)), :]
        data_train_y = data_y[: int(0.75 * len(data_y)), :]
        data_test = data[int(0.75 * len(data)):, :]
        data_test_y = data_y[int(0.75 * len(data_y)):, :]

        # Create the temporal dataset with a sequence length of 5

        if scaling:
            scaler_train = StandardScaler()
            scaler_y = StandardScaler()

            data_train = scaler_train.fit_transform(data_train)
            data_train_y = scaler_y.fit_transform(data_train_y)

            data_test = scaler_train.transform(data_test)
            data_test_y = scaler_y.transform(data_test_y)

        # Initialize and train the RandomForestRegressor
        # evals = [(data_train, data_train_y), (data_test, data_test_y)]

        # rf_regressor = XGBRegressor(random_state=42,
        #                             n_estimators=100,
        #                             subsample=0.6,
        #                             max_depth=3,
        #                             learning_rate=0.01, )
        # # rf_regressor.fit(data_train, data_train_y, eval_set=evals)
        # rf_regressor.fit(data_train, data_train_y)

        rf_regressor = XGBRegressor(random_state=42,
                                n_estimators=3000,
                                subsample=0.8,
                                max_depth=12,
                                learning_rate=0.001,)
        # rf_regressor.fit(data_train, data_train_y, eval_set=evals)
        rf_regressor.fit(data_train, data_train_y)

        grid = False
        if grid:
            param_grid = {
                'n_estimators': [100, 200, 500, 800],  # Number of boosting rounds
                'max_depth': [3, 5, 7, 10],  # Maximum tree depth
                'learning_rate': [0.01, 0.05, 0.1],  # Step size shrinkage
                'subsample': [0.6, 0.8, 1.0]  # Fraction of samples to be used for each tree
            }

            xgb_regressor = XGBRegressor(random_state=42)

            # Setup the GridSearchCV
            grid_search = GridSearchCV(estimator=xgb_regressor,
                                       param_grid=param_grid,
                                       scoring='neg_mean_absolute_error',
                                       cv=3,  # 3-fold cross-validation
                                       verbose=1,  # Print progress
                                       n_jobs=-1)  # Use all available cores

            # Fit the model to the training data
            grid_search.fit(data_train, data_train_y)

            # Print the best parameters found
            print(f"Best parameters: {grid_search.best_params_}")

            # Use the best estimator to make predictions
            rf_regressor = grid_search.best_estimator_

        y_pred_test = rf_regressor.predict(data_test)
        y_pred_train = rf_regressor.predict(data_train)

        if scaling:
            y_pred_test = scaler_y.inverse_transform(y_pred_test)
            data_test_y = scaler_y.inverse_transform(data_test_y)

            y_pred_train = scaler_y.inverse_transform(y_pred_train)
            data_train_y = scaler_y.inverse_transform(data_train_y)

        mse_test = mean_squared_error(data_test_y, y_pred_test)
        mae_test = mean_absolute_error(data_test_y, y_pred_test)

        mse_train = mean_squared_error(data_train_y, y_pred_train)
        mae_train = mean_absolute_error(data_train_y, y_pred_train)

        print(f"XGBRegressor Mean Squared Error test: {mse_test:.4f}")
        print(f"XGBRegressor Mean Absolut Error test: {mae_test:.4f}")

        print(f"XGBRegressor Mean Squared Error train: {mse_train:.4f}")
        print(f"XGBRegressor Mean Absolut Error train: {mae_train:.4f}")


        plot_pred(y_pred_test, data_test_y, seq + '_test_rolling_4000_arge2')
        plot_pred(y_pred_train, data_train_y, seq + '_train_rolling_4000_arge2')

        # target_parameters = pd.DataFrame([[78, 13, 86, 13]], columns=y)
        # target_parameterse = scaler_y.transform(target_parameters)
        # current_features = data_test[0]
        print('Done')


def main_4_models():
    scaling = True

    # Define the parameter grid for XGBRegressor

    # Train Model on these features
    x_train = ['0dbdbfbe-69a3-4ba6-b873-8893bca976a7', '5e3053f8-6d41-45ce-94e9-11e069f775a5',
               'e72186fc-d581-4368-917d-92ebd8124e5c', '2a4433b9-0f99-4cc2-9669-0b17eeaa62ab',
               '9265f861-e03e-4697-a132-ebfd93004827', '6b0c06fb-c6c8-49bc-a422-5b1ba2f7e4bc',
               'cf7314c0-c20c-4c53-b48f-debf29c87b43', '59997204-2b97-478b-9120-d1b3d784ad4d',
               '2acf6ab4-7288-4750-8c75-e04169ffc51e', '09545925-7c3e-4619-afe8-9b8ef5b01f24',
               '33d2504f-7719-43f0-9c1f-9a5dae89bc24', 'a36d9a05-6c14-4abf-aae9-4f9d09237a4d',
               '1310aedc-bcd7-429b-8aa9-2ed81d7533df', '5f22224c-8b0f-4985-bbf9-f5b01fda8d6b',
               'b16f6189-3804-4463-96fb-4bd444d0c276', '6289cd82-4bb2-4488-8975-4d2729bdfa9d',
               'bbb36936-1a05-406b-9178-2b790ca07a5d', '777f6918-12a2-4629-be75-5a669872f125',
               '50bfd169-8434-48b2-bf28-0a9665efdd84', 'f61fe432-3704-49cf-9559-ece9a1d45c5a',
               'eda51693-36df-4c68-8532-55bb21990351', '852d5936-7606-4bec-bbaf-9e92fa8b6145',
               '4d78ad32-b33b-4022-b6bc-4ba11e4544ef', '348b29c9-4a41-4efe-938f-3b54c2297c9f',
               '5101caad-35df-4d7f-8bce-80fefa4df866', '02ce84e5-d56f-40fd-8025-4dd85f5e2924',
               '45c84756-6a09-4040-80c6-15993de05bd4', '1115a47d-f41b-43e9-9804-3fa2f809b54a',
               'ed42f03f-09f6-4906-888e-10afd7871409', '30babb2b-84d1-4d7e-860f-2c1eae9ae177',
               'seconds']
    x_label = [
        'b0a51b71-7ba0-49a4-b2a2-13984d8c6616']  # , 'f022cf4f-dc76-4196-a41d-654ed24b2aa1', 'd85d28ca-fb7c-4ec3-951a-7972e5b62925']
    x_adaptable = ['5e3053f8-6d41-45ce-94e9-11e069f775a5', 'e72186fc-d581-4368-917d-92ebd8124e5c',
                   '2a4433b9-0f99-4cc2-9669-0b17eeaa62ab', '6b0c06fb-c6c8-49bc-a422-5b1ba2f7e4bc',
                   'cf7314c0-c20c-4c53-b48f-debf29c87b43', '59997204-2b97-478b-9120-d1b3d784ad4d',
                   '2acf6ab4-7288-4750-8c75-e04169ffc51e', '09545925-7c3e-4619-afe8-9b8ef5b01f24',
                   '33d2504f-7719-43f0-9c1f-9a5dae89bc24', '1310aedc-bcd7-429b-8aa9-2ed81d7533df',
                   'bbb36936-1a05-406b-9178-2b790ca07a5d', '777f6918-12a2-4629-be75-5a669872f125',
                   '50bfd169-8434-48b2-bf28-0a9665efdd84', 'eda51693-36df-4c68-8532-55bb21990351',
                   '4d78ad32-b33b-4022-b6bc-4ba11e4544ef', '348b29c9-4a41-4efe-938f-3b54c2297c9f',
                   '5101caad-35df-4d7f-8bce-80fefa4df866', '02ce84e5-d56f-40fd-8025-4dd85f5e2924',
                   '45c84756-6a09-4040-80c6-15993de05bd4', '1115a47d-f41b-43e9-9804-3fa2f809b54a',
                   'ed42f03f-09f6-4906-888e-10afd7871409']
    y = ['c3677f59-6137-4e8f-97f3-077ed6a62f2a', 'b37fd9cb-d968-4004-8afc-80a3522048a5',
         '870e0de0-2235-43bf-b309-101e6dfad86f', 'd08e8aba-87ad-47c6-b373-de74d73a351e']  # target outputs

    # All sequences
    # sequence_ids = ['MO112401', 'MO109301', 'MO111618', 'MO109174', 'MO111915', 'MO108058', 'MO109011', 'MO111624',
    #                 'MO112101', 'MO107772', 'MO109302', 'MO109161', 'MO109149', 'MO109068', 'MO109308', 'MO109914',
    #                 'MO109345', 'MO108912', 'MO109170', 'MO109089', 'MO109515', 'MO111365', 'MO109237', 'MO108801',
    #                 'MO109601', 'MO112102', 'MO109241', 'MO108980', 'MO111450', 'MO109759', 'MO109086', 'MO111616',
    #                 'MO107892', 'MO109169', 'MO108466', 'MO109586', 'MO111531', 'MO109409', 'MO109251', 'MO111867',
    #                 'MO108467', 'MO108799', 'MO109181', 'MO108792', 'MO109090', 'MO109745', 'MO108736', 'MO111630',
    #                 'MO109529', 'MO109091', 'MO108465', 'MO108794', 'MO111617', 'MO109805', 'MO111708', 'MO109171',
    #                 'MO109163', 'MO109830', 'MO108800', 'MO109407', 'MO111866', 'MO109758', 'MO108468', 'MO111591',
    #                 'MO109183', 'MO108384', 'MO109151', 'MO109077', 'MO109248', 'MO108795', 'MO109249', 'MO109176',
    #                 'MO111364', 'MO109425', 'MO108731', 'MO109087', 'MO109073', 'MO108874', 'MO112254', 'MO109162',
    #                 'MO109496', 'MO108668', 'MO109175', 'MO109424', 'MO109240', 'MO108544', 'MO109528', 'MO108737',
    #                 'MO112176']

    # Only 000063
    # sequence_ids = ['MO109151', 'MO108332', 'MO108333', 'MO108401', 'MO108407', 'MO108454', 'MO108534', 'MO108536',
    #                 'MO108726', 'MO108902', 'MO108980', 'MO109068', 'MO109073', 'MO109077', 'MO109151', 'MO109161',
    #                 'MO109162', 'MO109163', 'MO109240', 'MO109241', 'MO109301', 'MO109302', 'MO109345', 'MO109407',
    #                 'MO109409', 'MO109515', 'MO109528', 'MO109529', 'MO109586', 'MO109601', 'MO109745', 'MO109758',
    #                 'MO109759', 'MO109830', 'MO109914', 'MO111364', 'MO111365', 'MO111450', 'MO111591',
    #                 'MO111616', 'MO111617', 'MO111618', 'MO111624', 'MO111866', 'MO111867', 'MO111915', 'MO112101',
    #                 'MO112102', 'MO112176', 'MO112254']

    # # Only 000063:51
    sequence_ids = ['MO108332', 'MO108401', 'MO108534', 'MO108902', 'MO108980', 'MO109073', 'MO109161', 'MO109162',
                    'MO109301', 'MO109345', 'MO109407', 'MO109528', 'MO109529', 'MO109601', 'MO109758', 'MO111364',
                    'MO111365', 'MO111450', 'MO111616', 'MO111617', 'MO111618', 'MO111866', 'MO111915', 'MO112101',
                    'MO112176']

    for seq in sequence_ids:
        data, data_y = create_dataset([seq],
                                      x_train,
                                      x_label,
                                      y,
                                      rolling=True)

        # # Convert all string columns to categorical or numerical values
        # for col in data.select_dtypes(include=['object', 'string']).columns:
        #     # Convert to categorical codes if the column is categorical
        #     data[col] = data[col].astype('category').cat.codes

        label_encoder = LabelEncoder()
        data[x_label[0]] = label_encoder.fit_transform(data[x_label[0]])
        # data_test, data_test_y = create_dataset(['MO112102', 'MO112176', 'MO112254', 'MO112401'],
        data = data.values
        data_y = data_y.values

        plot_gt(data_y, seq + 'rolling')

        data, data_y = create_temporal_dataset(data, data_y, 10)

        n_samples, n_time_steps, n_features = data.shape
        data = data.reshape(n_samples, -1)  # Flatten to 2D for scaling

        data_train = data[: int(0.75 * len(data)), :]
        data_train_y = data_y[: int(0.75 * len(data_y)), :]
        data_test = data[int(0.75 * len(data)):, :]
        data_test_y = data_y[int(0.75 * len(data_y)):, :]

        # Create the temporal dataset with a sequence length of 5

        if scaling:
            scaler_train = StandardScaler()
            scaler_y = StandardScaler()

            data_train = scaler_train.fit_transform(data_train)
            data_train_y = scaler_y.fit_transform(data_train_y)

            data_test = scaler_train.transform(data_test)
            data_test_y = scaler_y.transform(data_test_y)

        # Initialize and train the RandomForestRegressor
        # evals = [(data_train, data_train_y), (data_test, data_test_y)]

        # rf_regressor = XGBRegressor(random_state=42,
        #                             n_estimators=100,
        #                             subsample=0.6,
        #                             max_depth=3,
        #                             learning_rate=0.01,)

        rf_regressor = SVR(kernel='rbf', C=1.0, epsilon=0.1)  # Adjust C and epsilon as needed



        # rf_regressor.fit(data_train, data_train_y, eval_set=evals)

        y_pred_test = []
        y_pred_train = []
        for cur_train in range(data_train_y.shape[1]):
            rf_regressor.fit(data_train, data_train_y[:, cur_train])

            grid = False
            if grid:
                param_grid = {
                    'n_estimators': [100, 200, 500, 800],  # Number of boosting rounds
                    'max_depth': [3, 5, 7, 10],  # Maximum tree depth
                    'learning_rate': [0.01, 0.05, 0.1],  # Step size shrinkage
                    'subsample': [0.6, 0.8, 1.0]  # Fraction of samples to be used for each tree
                }

                xgb_regressor = XGBRegressor(random_state=42)

                # Setup the GridSearchCV
                grid_search = GridSearchCV(estimator=xgb_regressor,
                                           param_grid=param_grid,
                                           scoring='neg_mean_absolute_error',
                                           cv=3,  # 3-fold cross-validation
                                           verbose=1,  # Print progress
                                           n_jobs=-1)  # Use all available cores

                # Fit the model to the training data
                grid_search.fit(data_train, data_train_y[:,cur_train])

                # Print the best parameters found
                print(f"Best parameters: {grid_search.best_params_}")

                # Use the best estimator to make predictions
                rf_regressor = grid_search.best_estimator_

            y_pred_test.append(rf_regressor.predict(data_test))
            y_pred_train.append(rf_regressor.predict(data_train))

        y_pred_test = np.swapaxes(np.array(y_pred_test),0,1)
        y_pred_train = np.swapaxes(np.array(y_pred_train),0,1)

        if scaling:
            y_pred_test = scaler_y.inverse_transform(y_pred_test)
            data_test_y = scaler_y.inverse_transform(data_test_y)

            y_pred_train = scaler_y.inverse_transform(y_pred_train)
            data_train_y = scaler_y.inverse_transform(data_train_y)

        mse_test = mean_squared_error(data_test_y, y_pred_test)
        mae_test = mean_absolute_error(data_test_y, y_pred_test)

        mse_train = mean_squared_error(data_train_y, y_pred_train)
        mae_train = mean_absolute_error(data_train_y, y_pred_train)

        print(f"XGBRegressor Mean Squared Error test: {mse_test:.4f}")
        print(f"XGBRegressor Mean Absolut Error test: {mae_test:.4f}")

        print(f"XGBRegressor Mean Squared Error train: {mse_train:.4f}")
        print(f"XGBRegressor Mean Absolut Error train: {mae_train:.4f}")

        plot_pred(y_pred_test, data_test_y, seq + '_test_rolling_single_mod')
        plot_pred(y_pred_train, data_train_y, seq + '_train_rolling_single_mod')

        # target_parameters = pd.DataFrame([[78, 13, 86, 13]], columns=y)
        # target_parameterse = scaler_y.transform(target_parameters)
        # current_features = data_test[0]
        print('Done')

def main_4_models_svm():
    scaling = True

    # Define the parameter grid for XGBRegressor

    # Train Model on these features
    x_train = ['0dbdbfbe-69a3-4ba6-b873-8893bca976a7', '5e3053f8-6d41-45ce-94e9-11e069f775a5',
               'e72186fc-d581-4368-917d-92ebd8124e5c', '2a4433b9-0f99-4cc2-9669-0b17eeaa62ab',
               '9265f861-e03e-4697-a132-ebfd93004827', '6b0c06fb-c6c8-49bc-a422-5b1ba2f7e4bc',
               'cf7314c0-c20c-4c53-b48f-debf29c87b43', '59997204-2b97-478b-9120-d1b3d784ad4d',
               '2acf6ab4-7288-4750-8c75-e04169ffc51e', '09545925-7c3e-4619-afe8-9b8ef5b01f24',
               '33d2504f-7719-43f0-9c1f-9a5dae89bc24', 'a36d9a05-6c14-4abf-aae9-4f9d09237a4d',
               '1310aedc-bcd7-429b-8aa9-2ed81d7533df', '5f22224c-8b0f-4985-bbf9-f5b01fda8d6b',
               'b16f6189-3804-4463-96fb-4bd444d0c276', '6289cd82-4bb2-4488-8975-4d2729bdfa9d',
               'bbb36936-1a05-406b-9178-2b790ca07a5d', '777f6918-12a2-4629-be75-5a669872f125',
               '50bfd169-8434-48b2-bf28-0a9665efdd84', 'f61fe432-3704-49cf-9559-ece9a1d45c5a',
               'eda51693-36df-4c68-8532-55bb21990351', '852d5936-7606-4bec-bbaf-9e92fa8b6145',
               '4d78ad32-b33b-4022-b6bc-4ba11e4544ef', '348b29c9-4a41-4efe-938f-3b54c2297c9f',
               '5101caad-35df-4d7f-8bce-80fefa4df866', '02ce84e5-d56f-40fd-8025-4dd85f5e2924',
               '45c84756-6a09-4040-80c6-15993de05bd4', '1115a47d-f41b-43e9-9804-3fa2f809b54a',
               'ed42f03f-09f6-4906-888e-10afd7871409', '30babb2b-84d1-4d7e-860f-2c1eae9ae177',
               'seconds']
    x_label = [
        'b0a51b71-7ba0-49a4-b2a2-13984d8c6616']  # , 'f022cf4f-dc76-4196-a41d-654ed24b2aa1', 'd85d28ca-fb7c-4ec3-951a-7972e5b62925']
    x_adaptable = ['5e3053f8-6d41-45ce-94e9-11e069f775a5', 'e72186fc-d581-4368-917d-92ebd8124e5c',
                   '2a4433b9-0f99-4cc2-9669-0b17eeaa62ab', '6b0c06fb-c6c8-49bc-a422-5b1ba2f7e4bc',
                   'cf7314c0-c20c-4c53-b48f-debf29c87b43', '59997204-2b97-478b-9120-d1b3d784ad4d',
                   '2acf6ab4-7288-4750-8c75-e04169ffc51e', '09545925-7c3e-4619-afe8-9b8ef5b01f24',
                   '33d2504f-7719-43f0-9c1f-9a5dae89bc24', '1310aedc-bcd7-429b-8aa9-2ed81d7533df',
                   'bbb36936-1a05-406b-9178-2b790ca07a5d', '777f6918-12a2-4629-be75-5a669872f125',
                   '50bfd169-8434-48b2-bf28-0a9665efdd84', 'eda51693-36df-4c68-8532-55bb21990351',
                   '4d78ad32-b33b-4022-b6bc-4ba11e4544ef', '348b29c9-4a41-4efe-938f-3b54c2297c9f',
                   '5101caad-35df-4d7f-8bce-80fefa4df866', '02ce84e5-d56f-40fd-8025-4dd85f5e2924',
                   '45c84756-6a09-4040-80c6-15993de05bd4', '1115a47d-f41b-43e9-9804-3fa2f809b54a',
                   'ed42f03f-09f6-4906-888e-10afd7871409']
    y = ['c3677f59-6137-4e8f-97f3-077ed6a62f2a', 'b37fd9cb-d968-4004-8afc-80a3522048a5',
         '870e0de0-2235-43bf-b309-101e6dfad86f', 'd08e8aba-87ad-47c6-b373-de74d73a351e']  # target outputs

    # All sequences
    # sequence_ids = ['MO112401', 'MO109301', 'MO111618', 'MO109174', 'MO111915', 'MO108058', 'MO109011', 'MO111624',
    #                 'MO112101', 'MO107772', 'MO109302', 'MO109161', 'MO109149', 'MO109068', 'MO109308', 'MO109914',
    #                 'MO109345', 'MO108912', 'MO109170', 'MO109089', 'MO109515', 'MO111365', 'MO109237', 'MO108801',
    #                 'MO109601', 'MO112102', 'MO109241', 'MO108980', 'MO111450', 'MO109759', 'MO109086', 'MO111616',
    #                 'MO107892', 'MO109169', 'MO108466', 'MO109586', 'MO111531', 'MO109409', 'MO109251', 'MO111867',
    #                 'MO108467', 'MO108799', 'MO109181', 'MO108792', 'MO109090', 'MO109745', 'MO108736', 'MO111630',
    #                 'MO109529', 'MO109091', 'MO108465', 'MO108794', 'MO111617', 'MO109805', 'MO111708', 'MO109171',
    #                 'MO109163', 'MO109830', 'MO108800', 'MO109407', 'MO111866', 'MO109758', 'MO108468', 'MO111591',
    #                 'MO109183', 'MO108384', 'MO109151', 'MO109077', 'MO109248', 'MO108795', 'MO109249', 'MO109176',
    #                 'MO111364', 'MO109425', 'MO108731', 'MO109087', 'MO109073', 'MO108874', 'MO112254', 'MO109162',
    #                 'MO109496', 'MO108668', 'MO109175', 'MO109424', 'MO109240', 'MO108544', 'MO109528', 'MO108737',
    #                 'MO112176']

    # Only 000063
    # sequence_ids = ['MO109151', 'MO108332', 'MO108333', 'MO108401', 'MO108407', 'MO108454', 'MO108534', 'MO108536',
    #                 'MO108726', 'MO108902', 'MO108980', 'MO109068', 'MO109073', 'MO109077', 'MO109151', 'MO109161',
    #                 'MO109162', 'MO109163', 'MO109240', 'MO109241', 'MO109301', 'MO109302', 'MO109345', 'MO109407',
    #                 'MO109409', 'MO109515', 'MO109528', 'MO109529', 'MO109586', 'MO109601', 'MO109745', 'MO109758',
    #                 'MO109759', 'MO109830', 'MO109914', 'MO111364', 'MO111365', 'MO111450', 'MO111591',
    #                 'MO111616', 'MO111617', 'MO111618', 'MO111624', 'MO111866', 'MO111867', 'MO111915', 'MO112101',
    #                 'MO112102', 'MO112176', 'MO112254']

    # # Only 000063:51
    sequence_ids = ['MO108332', 'MO108401', 'MO108534', 'MO108902', 'MO108980', 'MO109073', 'MO109161', 'MO109162',
                    'MO109301', 'MO109345', 'MO109407', 'MO109528', 'MO109529', 'MO109601', 'MO109758', 'MO111364',
                    'MO111365', 'MO111450', 'MO111616', 'MO111617', 'MO111618', 'MO111866', 'MO111915', 'MO112101',
                    'MO112176']

    for seq in sequence_ids[:1]:
        data, data_y = create_dataset([seq],
                                      x_train,
                                      x_label,
                                      y,
                                      rolling=True)

        # # Convert all string columns to categorical or numerical values
        # for col in data.select_dtypes(include=['object', 'string']).columns:
        #     # Convert to categorical codes if the column is categorical
        #     data[col] = data[col].astype('category').cat.codes

        label_encoder = LabelEncoder()
        data[x_label[0]] = label_encoder.fit_transform(data[x_label[0]])
        # data_test, data_test_y = create_dataset(['MO112102', 'MO112176', 'MO112254', 'MO112401'],
        data = data.values
        data_y = data_y.values

        plot_gt(data_y, seq + 'rolling')

        data, data_y = create_temporal_dataset(data, data_y, 10)

        n_samples, n_time_steps, n_features = data.shape
        data = data.reshape(n_samples, -1)  # Flatten to 2D for scaling

        data_train = data[: int(0.75 * len(data)), :]
        data_train_y = data_y[: int(0.75 * len(data_y)), :]
        data_test = data[int(0.75 * len(data)):, :]
        data_test_y = data_y[int(0.75 * len(data_y)):, :]

        # Create the temporal dataset with a sequence length of 5

        if scaling:
            scaler_train = StandardScaler()
            scaler_y = StandardScaler()

            data_train = scaler_train.fit_transform(data_train)
            data_train_y = scaler_y.fit_transform(data_train_y)

            data_test = scaler_train.transform(data_test)
            data_test_y = scaler_y.transform(data_test_y)

        # Initialize and train the RandomForestRegressor
        # evals = [(data_train, data_train_y), (data_test, data_test_y)]

        # rf_regressor = XGBRegressor(random_state=42,
        #                             n_estimators=100,
        #                             subsample=0.6,
        #                             max_depth=3,
        #                             learning_rate=0.01,)

        rf_regressor = SVR(kernel='linear',
                           C=100,
                           epsilon=0.1,
                           gamma='scale')  # Adjust C and epsilon as needed




        y_pred_test = []
        y_pred_train = []
        for cur_train in range(data_train_y.shape[1]):

            grid = False
            if grid:
                param_grid = {
                    'C': [0.1, 1, 10, 100],
                    'epsilon': [0.01, 0.1, 0.2, 0.5],
                    'kernel': ['linear', 'rbf', 'poly'],
                    'gamma': ['scale', 'auto']  # Kernel coefficient
                }
                svr = SVR()
                grid_search = GridSearchCV(estimator=svr,
                                           param_grid=param_grid,
                                           scoring='neg_mean_squared_error',
                                           # Use neg_mean_squared_error for regression
                                           cv=3,  # 3-fold cross-validation
                                           verbose=1,
                                           n_jobs=-1)

                # Setup the GridSearchCV

                # Fit the model to the training data
                grid_search.fit(data_train, data_train_y[:,cur_train])

                # Print the best parameters found
                print(f"Best parameters: {grid_search.best_params_}")

                # Use the best estimator to make predictions
                rf_regressor = grid_search.best_estimator_

            rf_regressor.fit(data_train, data_train_y[:, cur_train])

            y_pred_test.append(rf_regressor.predict(data_test))
            y_pred_train.append(rf_regressor.predict(data_train))

        y_pred_test = np.swapaxes(np.array(y_pred_test),0,1)
        y_pred_train = np.swapaxes(np.array(y_pred_train),0,1)

        if scaling:
            y_pred_test = scaler_y.inverse_transform(y_pred_test)
            data_test_y = scaler_y.inverse_transform(data_test_y)

            y_pred_train = scaler_y.inverse_transform(y_pred_train)
            data_train_y = scaler_y.inverse_transform(data_train_y)

        mse_test = mean_squared_error(data_test_y, y_pred_test)
        mae_test = mean_absolute_error(data_test_y, y_pred_test)

        mse_train = mean_squared_error(data_train_y, y_pred_train)
        mae_train = mean_absolute_error(data_train_y, y_pred_train)

        print(f"XGBRegressor Mean Squared Error test: {mse_test:.4f}")
        print(f"XGBRegressor Mean Absolut Error test: {mae_test:.4f}")

        print(f"XGBRegressor Mean Squared Error train: {mse_train:.4f}")
        print(f"XGBRegressor Mean Absolut Error train: {mae_train:.4f}")

        plot_pred(y_pred_test, data_test_y, seq + '_test_rolling_single_mod_svm')
        plot_pred(y_pred_train, data_train_y, seq + '_train_rolling_single_mod_svm')

        # target_parameters = pd.DataFrame([[78, 13, 86, 13]], columns=y)
        # target_parameterse = scaler_y.transform(target_parameters)
        # current_features = data_test[0]
        print('Done')


# main()
# main_4_models()