import torch
import torch.nn as nn
from torch.nn.utils.rnn import pad_sequence, pack_padded_sequence, pad_packed_sequence
from torch.utils.data import Dataset, DataLoader
import pandas as pd
import torch.optim as optim
from LSTM_dataloader import SensorDataset
from LSTM_model import SensorLSTM
import os  # For creating directories


# Collate function to pad sequences and handle targets
def collate_fn(batch):
    # Sort batch by sequence length (required for pack_padded_sequence)
    batch.sort(key=lambda x: len(x[0]), reverse=True)

    # Unzip the batch into inputs and targets
    inputs, targets = zip(*batch)

    # Pad the input sequences
    lengths = [len(seq) for seq in inputs]
    inputs_padded = pad_sequence(inputs, batch_first=True, padding_value=0)

    # Pad the target sequences
    targets_padded = pad_sequence(targets, batch_first=True, padding_value=0)

    return inputs_padded, targets_padded, lengths


# Define sequence_ids and base_path
sequence_ids = ['MO107772', 'MO109302', 'MO109251', 'MO109073', 'MO108668', 'MO109425', 'MO108794',
                'MO108736', 'MO108731', 'MO111624', 'MO108058', 'MO109171', 'MO109162', 'MO109759',
                'MO111630', 'MO107892', 'MO109745', 'MO109090', 'MO108799', 'MO111617', 'MO108792',
                'MO108801', 'MO108544', 'MO109424', 'MO109174', 'MO109151', 'MO109249', 'MO109089',
                'MO109237', 'MO109149', 'MO111364', 'MO108737', 'MO109496', 'MO109528', 'MO109241',
                'MO108465', 'MO109914', 'MO109586', 'MO108980', 'MO109011', 'MO109515', 'MO109183',
                'MO109163', 'MO108874', 'MO109161', 'MO108467', 'MO109301', 'MO111531', 'MO109175',
                'MO109248', 'MO109345', 'MO109409', 'MO111618', 'MO109169', 'MO111365', 'MO109407',
                'MO109240', 'MO109170', 'MO108468', 'MO109086', 'MO108384', 'MO108800', 'MO109308',
                'MO111708', 'MO109181', 'MO111616', 'MO109529', 'MO109601', 'MO111591', 'MO109091',
                'MO108795', 'MO108466', 'MO109176', 'MO111866', 'MO108912', 'MO109805', 'MO109830',
                'MO109087', 'MO109068', 'MO109758', 'MO111450']
base_path = '/home/robin/anomaly_detection_process_data/Masterflex-KI/new_sequences'

# Initialize dataset and dataloader
dataset = SensorDataset(sequence_ids, base_path)
dataloader = DataLoader(dataset, batch_size=5, shuffle=True, collate_fn=collate_fn)

# Model settings
input_size = 30  # Number of input features
hidden_size = 64  # LSTM hidden units
num_layers = 2  # Number of LSTM layers
output_size = 4  # Number of target outputs
learning_rate = 0.001
num_epochs = 20

# Instantiate the model and move to device
device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
model = SensorLSTM(input_size, hidden_size, num_layers, output_size).to(device)

# Define the loss function and optimizer
criterion = nn.MSELoss()
optimizer = optim.Adam(model.parameters(), lr=learning_rate)

# Directory to save the models
save_dir = "./saved_models"
os.makedirs(save_dir, exist_ok=True)

for epoch in range(num_epochs):
    model.train()

    for batch_inputs, batch_targets, lengths in dataloader:
        batch_inputs = batch_inputs.to(device)
        batch_targets = batch_targets.to(device)

        optimizer.zero_grad()

        # Forward pass
        outputs = model(batch_inputs, lengths)

        # Ensure the output shape matches the target shape
        if outputs.shape != batch_targets.shape:
            raise ValueError(f"Output shape {outputs.shape} does not match target shape {batch_targets.shape}")

        # Compute loss
        loss = criterion(outputs, batch_targets)

        loss.backward()
        optimizer.step()

    print(f'Epoch [{epoch + 1}/{num_epochs}], Loss: {loss.item():.4f}')

    # Save the model after every epoch
    model_path = os.path.join(save_dir, f"model_epoch_{epoch + 1}.pth")
    torch.save(model.state_dict(), model_path)
    print(f"Model saved at {model_path}")
