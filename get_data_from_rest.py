import os
from datetime import datetime, timezone
import requests
import pandas as pd
import numpy as np
from joblib import Parallel, delayed
import json
from threading import Lock
from collections import Counter
import matplotlib.pyplot as plt
from matplotlib import pyplot as plt


def get_data_from_api(api_url):
    try:
        response = requests.get(api_url, verify=False)
        # Check if the request was successful (status code 200)
        if response.status_code == 200:
            data = response.json()  # Convert response to JSON format
            return data
        else:
            print(f"Failed to retrieve data from API. Status code: {response.status_code}")
            return None
    except Exception as e:
        print(f"An error occurred: {e}")
        return None

def process_data(cur_ID):
    print(cur_ID)
    cur_interval_data = get_data_from_api('https://192.168.2.202/data/restbridge/v1/spans/intervals/' + cur_ID)
    #
    # started_at = pd.to_datetime(cur_interval_data['started_at'])
    # ended_at = pd.to_datetime(cur_interval_data['ended_at'])
    #
    # df = pd.DataFrame(index=pd.date_range(start=started_at, end=ended_at, freq='s'))
    all_bindings = []
    for entry in cur_interval_data['bindings']:
        binding_id = entry['bindingid']
        all_bindings.append(binding_id)
    return np.unique(all_bindings)

def get_intervall_dict(relevant_Ids):
    # Function to process data and update dictionary
    def process_data_and_update_dict(cur_ID):
        print(cur_ID)
        cur_interval_data = get_data_from_api('https://192.168.2.202/data/restbridge/v1/spans/intervals/' + cur_ID + '?leading=true')

        mydict = [cur_ID, cur_interval_data]
        return mydict

    # Parallelize the loop
    num_cores = 15
    all_all_dicts = []
    bins = np.arange(0, len(relevant_Ids), 10)

    if len(bins) > 0:
        for k in range(len(bins) - 1):
            all_dicts = Parallel(n_jobs=num_cores)(
                delayed(process_data_and_update_dict)(cur_ID) for cur_ID in relevant_Ids[bins[k]:bins[k + 1]])
            all_all_dicts.append(all_dicts)

    all_dicts = Parallel(n_jobs=num_cores)(
        delayed(process_data_and_update_dict)(cur_ID) for cur_ID in relevant_Ids[bins[-1]:len(relevant_Ids)])
    all_all_dicts.append(all_dicts)

    final_dict = []
    for k in range(len(all_all_dicts)):
        for l in range(len(all_all_dicts[k])):
            final_dict.append(all_all_dicts[k][l])

    return final_dict

def get_bindings_in_sequences(sequences):
    all_bindings = []
    for cur_dict in sequences:
        if 'bindings' in cur_dict[1]:
            bindings = []
            for entry in cur_dict[1]['bindings']:
                binding_id = entry['bindingid']
                bindings.append(binding_id)
            if len(bindings) > 0:
                bindings = [cur_dict[0], np.unique(bindings)]
            else:
                bindings = [cur_dict[0], []]
        else:
            bindings = [cur_dict[0], []]
        all_bindings.append(bindings)
    return all_bindings

def seq2df_old(seq):
    def process_binding(cur_binding, seq_df):
        if cur_binding:
            time = pd.to_datetime(cur_binding['occured_at']).round('1s')
            binding = cur_binding['bindingid']
            value = cur_binding['value']
            if pd.isna(seq_df.loc[time, binding]):
                seq_df.loc[time, binding] = value

    df_columns = pd.read_csv('Masterflex-KI/Index.csv')
    # Extract the column names as a list
    column_names = df_columns['Column_Names'].tolist()
    started_at = pd.to_datetime(seq[1]['started_at'])
    ended_at = pd.to_datetime(seq[1]['ended_at'])
    seq_df = pd.DataFrame(columns=column_names[1:], index=pd.date_range(start=started_at, end=ended_at, freq='s'))
    seq_df['seconds'] = np.arange(0, len(seq_df))
    if 'bindings' in seq[1]:
        Parallel(n_jobs=-1)(delayed(process_binding)(cur_binding, seq_df) for cur_binding in seq[1]['bindings'])
    seq_df.replace(method='ffill', inplace=True)

    return [seq[0], seq_df]

def fill_df(seq, df=None):

    for cur_time, cur_bindings in seq['timeData'].items():
        cur_time_stamp = pd.to_datetime(cur_time).round(freq='s')
        for cur_binding in cur_bindings:
            # Update values in seq_df directly without using .loc[]
            df.at[cur_time_stamp, cur_binding[0]] = cur_binding[1]

    return df


def finalize_df(df):
    # Forward-fill missing values in the DataFrame
    df.fillna(method='ffill', inplace=True)

    # Load the mapping file, handle possible file loading errors
    try:
        mapping = pd.read_excel('Masterflex-KI/Mapping.xlsx')
    except FileNotFoundError:
        raise FileNotFoundError("The mapping file 'Masterflex-KI/Mapping.xlsx' could not be found.")

    # Ensure 'BindingId_L5' and 'Name' columns exist in the mapping DataFrame
    if 'Binding_L5' not in mapping.columns or 'Name' not in mapping.columns:
        raise ValueError("The mapping file must contain 'Binding_L5' and 'Name' columns.")

    # Generate new column names by mapping from Binding_L5 to Name
    new_cols = []
    for cur_column in df.columns[:-1]:  # Exclude the last column ('seconds')
        matching_name = mapping['Name'][mapping['Binding_L5'] == cur_column]
        if not matching_name.empty:
            new_cols.append(str(matching_name.iloc[0]))  # Get the first matching name
        else:
            new_cols.append(cur_column)  # If no match, keep the original column name

    # Add 'seconds' column back
    new_cols.append('seconds')

    # Clean column names in case of unexpected newline characters
    new_cols = [entry.split('\n')[0] for entry in new_cols]

    # Assign the new column names to the DataFrame
    df.columns = new_cols

    # Remove rows before the first occurrence of a specific condition in a column
    condition_col = 'L6-Tagebuch-Titel'  # Column ID
    condition_value = 'Bearbeitung'  # Target value to find in this column

    if condition_col in df.columns:
        first_occurrence = df[df[condition_col] == condition_value].index.min()
        if pd.notna(first_occurrence):
            df = df.loc[first_occurrence:]  # Keep only rows from the first occurrence onward

            # Reset 'seconds' column to start at 0
            df['seconds'] = df['seconds'] - df['seconds'].iloc[0]  # Subtract the initial value of 'seconds'
    else:
        print(f"Warning: Column '{condition_col}' not found in the DataFrame. No rows will be removed.")

    return df


def get_values_swagger(url = 'https://192.168.2.202/vipra/api/v4/uidata/values',
                       headers={
                           'accept': '*/*',
                           'Content-Type': 'application/json'
                       },
                       bindings=["1d0cea99-ccc2-4cec-b3b9-74a9a27adf4f"],
                       start = "2024-03-05T09:37:04Z",
                       end = "2024-03-05T09:37:40Z"
                       ):
    # bindings = ["1d0cea99-ccc2-4cec-b3b9-74a9a27adf4f"]#,
                # "af4e4d10-c9a0-4d0c-830b-63c313e37477",
                # "152e43d1-1f46-4e36-805d-6dffa01d59d4",
                # "38fcd451-38cc-4659-9c23-38eb7e8adc00",
                # "9e125229-51f6-4e86-81b1-77d434d6b566",
                # "677ab75c-8a56-434a-9d78-eec9cef0d7d2",
                # "31b45a7e-e896-40ba-a419-ee88241e7463",
                # "fc47dd77-0f1e-4b71-84ec-a5632406e2d7",
                # "c5f270cc-0704-48db-a28f-d1585255cb2a",
                # "a6e855f8-84e0-459b-b5d4-32421c0d7fb6",
                # "abd8ca56-8334-4bfb-a899-fb5effa1fbc4",
                # "bc38e03c-119b-476b-a7ab-f4f0787d288e",
                # "50b2dc06-ebb5-4296-8009-f0c4b4ffe747",
                # "8f677d65-dd15-4148-a0e1-8b799aac9bc2",
                # "5593ba64-487d-46ae-8b97-00a2e8f8380c",
                # "714a44d5-cfae-4170-8a07-b409d1313e9b",
                # "e1058fb2-6440-4f47-a264-4726f2ccf1ba",
                # "2c71ddbb-edab-4725-ad3e-21815c1d5f3c",
                # "2957a948-9e5f-45cf-97a2-b71c996289ee",
                # "db000026-55d7-4651-8611-a68137a84b3b",
                # "2a169f23-3aff-4d62-9f7d-639ec777f2f6",
                # "6866d1db-28db-4813-a623-8f861692c735",
                # "baefdee2-e048-4241-8953-a18d8235a110",
                # "19ca2480-3466-4799-8277-f69390f22862",
                # "e15399ae-aa36-46b1-8e06-10d965a44cfe",
                # "b0c4d9b6-174e-4ee9-93ad-18e381b77cee",
                # "945418bb-ccab-403e-b1da-a663937f2289",
                # "f9f53088-ce80-4823-962e-a40f40af08a4",
                # "2b234ca8-f3da-47da-9039-00d56492b28d",
                # "88c52509-7d3f-42e5-b632-91e28dd316f6",
                # "05b4f682-394d-484c-98ad-4a276d090a88",
                # "0d0a5ed0-b035-4e5b-b511-3b1a2d9eb98b",
                # "b150c95a-5169-406f-8126-845f98dfc33f",
                # "dfcfa197-e7f2-4e24-b0b2-1a56d2484768",
                # "c4ed96a8-cb7e-49ec-b5de-d33d131fbb5a",
                # "eb435a9e-8c2d-42bf-8720-ff37e6f0f34e",
                # "39af68c8-dd9e-4e9b-ace8-e7664ddcf44a",
                # "1dff7396-618f-48d3-95d5-c75644998fdb"]
    data = {
        "bindingIds": bindings,
        "from": start,
        "to": end,
        "reduced": False,
        "startWithLastValue": True,
        "endWithLastValue": False,
    }

    response = requests.post(url, headers=headers, data=json.dumps(data), verify=False)


    return response.json()

def make_empty_df(start="2024-03-05T09:37:04Z", end="2024-03-05T09:37:40Z"):

    df_columns = pd.read_csv('Masterflex-KI/Index_5.csv')
    # Extract the column names as a list
    column_names = df_columns['Column_Names'].tolist()
    started_at = pd.to_datetime(start)
    ended_at = pd.to_datetime(end)
    seq_df = pd.DataFrame(columns=column_names[1:], index=pd.date_range(start=started_at, end=ended_at, freq='s'))
    seq_df['seconds'] = np.arange(0, len(seq_df))
    return seq_df

def reduce_sequence(seq):
    reduced_seq = {}

    last_second = None

    for timestamp, data in seq['timeData'].items():
        current_second = datetime.fromisoformat(timestamp[:-1]).replace(microsecond=0)
        if current_second != last_second:
            reduced_seq[timestamp] = data
            last_second = current_second
    seq['timeData'] = reduced_seq
    return seq

def main_context():

    # url for distinct span id
    # api_url = "https://192.168.2.202/vipra/api/v4/datadealer/interval/list?spanid=85268a56-7ada-482b-865f-b5647eaa3bcf"
    api_url = "https://192.168.2.202/vipra/api/v4/uidata/interval/list?size=2000&spanid=85268a56-7ada-482b-865f-b5647eaa3bcf&bindingvaluesreduceaggregationmethod=AVERAGE"
    # api_url = "https://192.168.2.202/vipra/api/v4/datadealer/interval/list?spanid=0eaf5be6-2962-43fb-b4a1-f09093bcd7c6"
    # api_url = "https://192.168.2.202/vipra/api/v4/uidata/interval/list?size=1000&spanid=0eaf5be6-2962-43fb-b4a1-f09093bcd7c6&bindingvaluesreduceaggregationmethod=AVERAGE"

    data = get_data_from_api(api_url)
    data = pd.DataFrame(data)
    bindings_6 = ["1d0cea99-ccc2-4cec-b3b9-74a9a27adf4f",
                  "af4e4d10-c9a0-4d0c-830b-63c313e37477",
                  "152e43d1-1f46-4e36-805d-6dffa01d59d4",
                  "38fcd451-38cc-4659-9c23-38eb7e8adc00",
                  "9e125229-51f6-4e86-81b1-77d434d6b566",
                  "677ab75c-8a56-434a-9d78-eec9cef0d7d2",
                  "31b45a7e-e896-40ba-a419-ee88241e7463",
                  "fc47dd77-0f1e-4b71-84ec-a5632406e2d7",
                  "c5f270cc-0704-48db-a28f-d1585255cb2a",
                  "a6e855f8-84e0-459b-b5d4-32421c0d7fb6",
                  "abd8ca56-8334-4bfb-a899-fb5effa1fbc4",
                  "bc38e03c-119b-476b-a7ab-f4f0787d288e",
                  "50b2dc06-ebb5-4296-8009-f0c4b4ffe747",
                  "8f677d65-dd15-4148-a0e1-8b799aac9bc2",
                  "5593ba64-487d-46ae-8b97-00a2e8f8380c",
                  "714a44d5-cfae-4170-8a07-b409d1313e9b",
                  "e1058fb2-6440-4f47-a264-4726f2ccf1ba",
                  "2c71ddbb-edab-4725-ad3e-21815c1d5f3c",
                  "2957a948-9e5f-45cf-97a2-b71c996289ee",
                  "db000026-55d7-4651-8611-a68137a84b3b",
                  "2a169f23-3aff-4d62-9f7d-639ec777f2f6",
                  "6866d1db-28db-4813-a623-8f861692c735",
                  "baefdee2-e048-4241-8953-a18d8235a110",
                  "19ca2480-3466-4799-8277-f69390f22862",
                  "e15399ae-aa36-46b1-8e06-10d965a44cfe",
                  "b0c4d9b6-174e-4ee9-93ad-18e381b77cee",
                  "945418bb-ccab-403e-b1da-a663937f2289",
                  "f9f53088-ce80-4823-962e-a40f40af08a4",
                  "2b234ca8-f3da-47da-9039-00d56492b28d",
                  "88c52509-7d3f-42e5-b632-91e28dd316f6",
                  "05b4f682-394d-484c-98ad-4a276d090a88",
                  "0d0a5ed0-b035-4e5b-b511-3b1a2d9eb98b",
                  "b150c95a-5169-406f-8126-845f98dfc33f",
                  "dfcfa197-e7f2-4e24-b0b2-1a56d2484768",
                  "c4ed96a8-cb7e-49ec-b5de-d33d131fbb5a",
                  "eb435a9e-8c2d-42bf-8720-ff37e6f0f34e",
                  "39af68c8-dd9e-4e9b-ace8-e7664ddcf44a",
                  "1dff7396-618f-48d3-95d5-c75644998fdb",
                  "6d5e4fb8-7be7-4ce8-b00d-891f116353b4",
                  "ba926976-de42-4acf-8b09-6db887c104b3",
                  "36150149-a110-4394-9674-d83ec43565d7"]

    bindings_5 = ["0dbdbfbe-69a3-4ba6-b873-8893bca976a7",
                  "5e3053f8-6d41-45ce-94e9-11e069f775a5",
                  "b0a51b71-7ba0-49a4-b2a2-13984d8c6616",
                  "e72186fc-d581-4368-917d-92ebd8124e5c",
                  "2a4433b9-0f99-4cc2-9669-0b17eeaa62ab",
                  "9265f861-e03e-4697-a132-ebfd93004827",
                  "f022cf4f-dc76-4196-a41d-654ed24b2aa1",
                  "d85d28ca-fb7c-4ec3-951a-7972e5b62925",
                  "6b0c06fb-c6c8-49bc-a422-5b1ba2f7e4bc",
                  "cf7314c0-c20c-4c53-b48f-debf29c87b43",
                  "59997204-2b97-478b-9120-d1b3d784ad4d",
                  "9cdf168a-7faf-4cba-977d-d9141c6af403",
                  "2acf6ab4-7288-4750-8c75-e04169ffc51e",
                  "c3677f59-6137-4e8f-97f3-077ed6a62f2a",
                  "09545925-7c3e-4619-afe8-9b8ef5b01f24",
                  "33d2504f-7719-43f0-9c1f-9a5dae89bc24",
                  "a36d9a05-6c14-4abf-aae9-4f9d09237a4d",
                  "b37fd9cb-d968-4004-8afc-80a3522048a5",
                  "1310aedc-bcd7-429b-8aa9-2ed81d7533df",
                  "5f22224c-8b0f-4985-bbf9-f5b01fda8d6b",
                  "00734f24-6930-4325-b2be-499b167a11bf",
                  "b16f6189-3804-4463-96fb-4bd444d0c276",
                  "6289cd82-4bb2-4488-8975-4d2729bdfa9d",
                  "b637208f-32f7-4cf6-a551-6ddee2e8c450",
                  "bbb36936-1a05-406b-9178-2b790ca07a5d",
                  "777f6918-12a2-4629-be75-5a669872f125",
                  "50bfd169-8434-48b2-bf28-0a9665efdd84",
                  "f61fe432-3704-49cf-9559-ece9a1d45c5a",
                  "eda51693-36df-4c68-8532-55bb21990351",
                  "852d5936-7606-4bec-bbaf-9e92fa8b6145",
                  "4d78ad32-b33b-4022-b6bc-4ba11e4544ef",
                  "348b29c9-4a41-4efe-938f-3b54c2297c9f",
                  "870e0de0-2235-43bf-b309-101e6dfad86f",
                  "5101caad-35df-4d7f-8bce-80fefa4df866",
                  "02ce84e5-d56f-40fd-8025-4dd85f5e2924",
                  "b637208f-32f7-4cf6-a551-6ddee2e8c450",
                  "d08e8aba-87ad-47c6-b373-de74d73a351e",
                  "45c84756-6a09-4040-80c6-15993de05bd4",
                  "1115a47d-f41b-43e9-9804-3fa2f809b54a",
                  "ed42f03f-09f6-4906-888e-10afd7871409",
                  "30babb2b-84d1-4d7e-860f-2c1eae9ae177"]

    mapping = pd.read_excel('/home/robin/anomaly_detection_process_data/Masterflex-KI/Mapping.xlsx')
    # Get relevant IntevalIDs (000007:1) so just for one article and diameter >100 (i guess)
    iterrator = 0
    relevant_Ids = []
    for index, row in data.iterrows():

        # Compare with a timezone-aware datetime object
        if datetime.fromisoformat(row['intervalList']['started_at'].replace('Z', '+00:00')) > datetime(2024, 5, 1, 0, 0, 0, tzinfo=timezone.utc):
            # print(row['intervalList']['started_at'])

            if row.intervalList['context'] is not None:
                if '000063' in row.intervalList['context']['Artikelnummer']:
                    print(row.intervalList['context']['Artikelnummer'])
                    relevant_Ids.append([row.intervalList['seq'], row.intervalList['id'], row.intervalList['started_at'], row.intervalList['ended_at'], row.intervalList['context']])
                    iterrator+=1
        # relevant_Ids = relevant_Ids[2:]
# 'f022cf4f-dc76-4196-a41d-654ed24b2aa1'
    for cur_id in relevant_Ids[1:]:
        print(cur_id)

        try:
            # Check if cur_id[4] is not None and contains the required key
            if cur_id[4] is not None and 'L5-Produktionsplanung-PPS_ORDER' in cur_id[4]:
                order_value = str(cur_id[4]['L5-Produktionsplanung-PPS_ORDER'])
                dir_path = f'Masterflex-KI/new_sequences/{order_value}'

                # Check if the directory exists, if not, process further
                if not os.path.exists(dir_path):
                    print(order_value)

                    # Create an empty dataframe between cur_id[2] and cur_id[3]
                    df = make_empty_df(start=cur_id[2], end=cur_id[3])
                    all_responses = []

                    # Process bindings_5 list
                    for cur_binding in bindings_5:
                        response = get_values_swagger(start=cur_id[2], end=cur_id[3], bindings=[cur_binding])

                        # Loop while response has max timeData (2000 entries)
                        while len(response['timeData']) == 2000:
                            print(len(response['timeData']))
                            red_response = reduce_sequence(response)
                            df = fill_df(red_response, df)
                            all_responses.append(red_response)
                            response = get_values_swagger(start=cur_id[2], end=next(iter(response['timeData'])),
                                                          bindings=[cur_binding])

                        # Process final response
                        red_response = reduce_sequence(response)
                        df = fill_df(red_response, df)
                        all_responses.append(red_response)

                    # Create directory if it doesn't exist
                    if not os.path.exists(dir_path):
                        os.mkdir(dir_path)

                    # Save the dataframe as CSV and Excel
                    df.to_csv(f'{dir_path}/features.csv', index=False)
                    df.to_excel(f'{dir_path}/features.xlsx', index=False)

            else:
                print(f"cur_id[4] is either None or missing 'L5-Produktionsplanung-PPS_ORDER': {cur_id}")

        except TypeError as e:
            print(f"TypeError: {e} - cur_id[4] might not be subscriptable or missing expected key")

        except KeyError as e:
            print(f"KeyError: {e} - Key not found in the dictionary")

        except Exception as e:
            print(f"An error occurred: {e}")

def get_stats(relevant_Ids):
    def calculate_duration(start, end):
        if start and end:

            # Parse start time (no fractional seconds expected)
            start_time = datetime.strptime(start, '%Y-%m-%dT%H:%M:%SZ')
            # Handle fractional seconds in the end time
            try:
                # Try parsing end time normally (no fractional seconds)
                end_time = datetime.strptime(end, '%Y-%m-%dT%H:%M:%SZ')

            except ValueError:

                # Parse end time with fractional seconds

                end_time = datetime.strptime(end.split(".")[0], '%Y-%m-%dT%H:%M:%S')

            # Calculate duration in hours

            return (end_time - start_time).total_seconds() / 3600

        return None

    # Extract order IDs and durations again

    order_ids = []
    durations = []
    for entry in relevant_Ids:
        start_time = entry[2]
        end_time = entry[3]
        details = entry[4]
        if details and "L5-Produktionsplanung-PPS_ORDER" in details:
            order_id = details["L5-Produktionsplanung-PPS_ORDER"]
            duration = calculate_duration(start_time, end_time)

            if duration is not None:
                order_ids.append(order_id)

                durations.append(duration)

    # Plotting the bar plot

    plt.figure(figsize=(10, 6))
    plt.bar(order_ids, durations, color='skyblue')
    plt.xlabel('Order ID')
    plt.ylabel('Duration (hours)')
    plt.title('Product Duration (in hours) for each L5-Produktionsplanung-PPS_ORDER')
    plt.xticks(rotation=90)
    plt.tight_layout()
    plt.savefig('Durations.png')
    plt.show()
    # Extracting the 'Artikelnummer' values and corresponding durations
    artikelnummer_prefixes = []

    # Extracting the relevant information
    artikelnummer_values = []

    for entry in relevant_Ids:
        # Check if 'L5-Produktionsplanung-PPS_ORDER' exists
        if isinstance(entry[4], dict) and 'L5-Produktionsplanung-PPS_ORDER' in entry[4]:
            artikelnummer = entry[4]['Artikelnummer'].split(':')[0]  # Get the first part (e.g., '000063')
            artikelnummer_values.append(artikelnummer)  # Collect for frequency counting

    # Count the occurrences of each 'Artikelnummer' value
    artikelnummer_counter = Counter(artikelnummer_values)

    # Extract values and counts for plotting
    values = list(artikelnummer_counter.keys())
    counts = list(artikelnummer_counter.values())

    # Plotting the histogram (as a bar plot since histograms work with continuous data)
    plt.figure(figsize=(10, 6))
    plt.bar(values, counts, color='skyblue')
    plt.xlabel('Artikelnummer (first part)')
    plt.ylabel('Frequency')
    plt.title('Histogram of Artikelnummer First Part Occurrences')
    plt.xticks(rotation=45, ha='right')
    plt.tight_layout()
    plt.savefig('Product_count.png')
    plt.show()

    # Extracting the second part of 'Artikelnummer'
    artikelnummer_second_part_values = []

    for entry in relevant_Ids:
        # Check if 'L5-Produktionsplanung-PPS_ORDER' exists
        if isinstance(entry[4], dict) and 'L5-Produktionsplanung-PPS_ORDER' in entry[4]:
            artikelnummer_split = entry[4]['Artikelnummer'].split(':')  # Split the 'Artikelnummer' by ':'

            # Ensure there are at least two parts in 'Artikelnummer'
            if len(artikelnummer_split) > 1:
                artikelnummer_second_part = artikelnummer_split[1]  # Get the second part
                artikelnummer_second_part_values.append(artikelnummer_second_part)  # Collect for frequency counting

    # Count the occurrences of each 'Artikelnummer' second part
    artikelnummer_second_part_counter = Counter(artikelnummer_second_part_values)

    # Extract values and counts for plotting
    values = list(artikelnummer_second_part_counter.keys())
    counts = list(artikelnummer_second_part_counter.values())

    # Plotting the histogram (as a bar plot for categorical data)
    plt.figure(figsize=(10, 6))
    plt.bar(values, counts, color='skyblue')
    plt.xlabel('Artikelnummer (second part)')
    plt.ylabel('Frequency')
    plt.title('Histogram of Artikelnummer Second Part Occurrences')
    plt.xticks(rotation=45, ha='right')
    plt.tight_layout()
    plt.savefig('Diameter.png')
    plt.show()
def main_all():

    # url for distinct span id
    # api_url = "https://192.168.2.202/vipra/api/v4/datadealer/interval/list?spanid=85268a56-7ada-482b-865f-b5647eaa3bcf"
    api_url = "https://192.168.2.202/vipra/api/v4/uidata/interval/list?size=2000&spanid=85268a56-7ada-482b-865f-b5647eaa3bcf&bindingvaluesreduceaggregationmethod=AVERAGE"


    data = get_data_from_api(api_url)
    data = pd.DataFrame(data)
    bindings = ["1d0cea99-ccc2-4cec-b3b9-74a9a27adf4f",
                "af4e4d10-c9a0-4d0c-830b-63c313e37477",
                "152e43d1-1f46-4e36-805d-6dffa01d59d4",
                "38fcd451-38cc-4659-9c23-38eb7e8adc00",
                "9e125229-51f6-4e86-81b1-77d434d6b566",
                "677ab75c-8a56-434a-9d78-eec9cef0d7d2",
                "31b45a7e-e896-40ba-a419-ee88241e7463",
                "fc47dd77-0f1e-4b71-84ec-a5632406e2d7",
                "c5f270cc-0704-48db-a28f-d1585255cb2a",
                "a6e855f8-84e0-459b-b5d4-32421c0d7fb6",
                "abd8ca56-8334-4bfb-a899-fb5effa1fbc4",
                "bc38e03c-119b-476b-a7ab-f4f0787d288e",
                "50b2dc06-ebb5-4296-8009-f0c4b4ffe747",
                "8f677d65-dd15-4148-a0e1-8b799aac9bc2",
                "5593ba64-487d-46ae-8b97-00a2e8f8380c",
                "714a44d5-cfae-4170-8a07-b409d1313e9b",
                "e1058fb2-6440-4f47-a264-4726f2ccf1ba",
                "2c71ddbb-edab-4725-ad3e-21815c1d5f3c",
                "2957a948-9e5f-45cf-97a2-b71c996289ee",
                "db000026-55d7-4651-8611-a68137a84b3b",
                "2a169f23-3aff-4d62-9f7d-639ec777f2f6",
                "6866d1db-28db-4813-a623-8f861692c735",
                "baefdee2-e048-4241-8953-a18d8235a110",
                "19ca2480-3466-4799-8277-f69390f22862",
                "e15399ae-aa36-46b1-8e06-10d965a44cfe",
                "b0c4d9b6-174e-4ee9-93ad-18e381b77cee",
                "945418bb-ccab-403e-b1da-a663937f2289",
                "f9f53088-ce80-4823-962e-a40f40af08a4",
                "2b234ca8-f3da-47da-9039-00d56492b28d",
                "88c52509-7d3f-42e5-b632-91e28dd316f6",
                "05b4f682-394d-484c-98ad-4a276d090a88",
                "0d0a5ed0-b035-4e5b-b511-3b1a2d9eb98b",
                "b150c95a-5169-406f-8126-845f98dfc33f",
                "dfcfa197-e7f2-4e24-b0b2-1a56d2484768",
                "c4ed96a8-cb7e-49ec-b5de-d33d131fbb5a",
                "eb435a9e-8c2d-42bf-8720-ff37e6f0f34e",
                "39af68c8-dd9e-4e9b-ace8-e7664ddcf44a",
                "1dff7396-618f-48d3-95d5-c75644998fdb",
                "6d5e4fb8-7be7-4ce8-b00d-891f116353b4",
                "ba926976-de42-4acf-8b09-6db887c104b3",
                "36150149-a110-4394-9674-d83ec43565d7"]

    # Get relevant IntevalIDs (000007:1) so just for one article and diameter >100 (i guess)
    iterrator = 0
    relevant_Ids = []
    for index, row in data.iterrows():
        if row.intervalList['context'] is not None:
            relevant_Ids.append([row.intervalList['seq'], row.intervalList['id'], row.intervalList['started_at'], row.intervalList['ended_at'], row.intervalList['context']])
            iterrator+=1

    filtered_relevant_Ids = []
    to_long_Ids = []

    for item in relevant_Ids:
        start_time = datetime.fromisoformat(item[2][:-1])  # Remove 'Z' at the end before parsing
        end_time = datetime.fromisoformat(item[3][:-1])  # Remove 'Z' at the end before parsing
        duration = end_time - start_time
        if duration.days <= 2:
            filtered_relevant_Ids.append(item)
        else:
            to_long_Ids.append(item)

    relevant_Ids = filtered_relevant_Ids


    for cur_id in relevant_Ids:
        df = make_empty_df(start=cur_id[2],end=cur_id[3])
        all_responnses = []
        for cur_binding in bindings:
            response = get_values_swagger(start=cur_id[2],end=cur_id[3], bindings=[cur_binding])
            print(response['timeData'])
            while len(response['timeData']) == 2000:
                print(len(response['timeData']))
                red_response = reduce_sequence(response)
                df = fill_df(red_response, df)
                all_responnses.append(red_response)
                response = get_values_swagger(start=cur_id[2], end=next(iter(response['timeData'])), bindings=[cur_binding])

            red_response = reduce_sequence(response)
            df = fill_df(red_response, df)

            all_responnses.append(red_response)
        df = finalize_df(df)

        if not os.path.exists('Masterflex-KI/all_sequences/' + str(cur_id[0])):
            os.mkdir('Masterflex-KI/all_sequences/' + str(cur_id[0]))
        df.to_csv('Masterflex-KI/all_sequences/' + str(cur_id[0]) + '/features' + '.csv', index=False)


        df.to_excel('Masterflex-KI/all_sequences/' + str(cur_id[0]) + '/features' + '.xlsx', index=False)

        # i = 0
        # print(cur_id[0])
        # for cur_col in df.columns:
        #     plt.plot(df[cur_col])
        #     plt.title(cur_col)
        #     plt.savefig('Masterflex-KI/all_sequences/' + str(cur_id[0]) + '/' + str(i) + '.png')
        #     plt.close()
        #     i += 1

# main_context()

def transform_seq(): #'MO109149', , 'MO109805'
    subjects = ['MO108331', 'MO108332', 'MO108333', 'MO108401', 'MO108407', 'MO108454', 'MO108534', 'MO108536', 'MO108726', 'MO108902', 'MO108980', 'MO109068', 'MO109073', 'MO109077',  'MO109151', 'MO109161', 'MO109162', 'MO109163', 'MO109240', 'MO109241', 'MO109301', 'MO109302', 'MO109345', 'MO109407', 'MO109409', 'MO109515', 'MO109528', 'MO109529', 'MO109586', 'MO109601', 'MO109745', 'MO109758', 'MO109759', 'MO109830', 'MO109914', 'MO111364', 'MO111365', 'MO111450', 'MO111531', 'MO111591', 'MO111616', 'MO111617', 'MO111618', 'MO111624', 'MO111866', 'MO111867', 'MO111915', 'MO112101', 'MO112102', 'MO112176', 'MO112254', 'MO112401']
    max_num = 0
    for cur_subj in subjects:
        print(cur_subj)
        if os.path.exists(f'/home/robin/anomaly_detection_process_data/Masterflex-KI/63_new/{cur_subj}/features.xlsx'):
            df = pd.read_excel(f'/home/robin/anomaly_detection_process_data/Masterflex-KI/63_new/{cur_subj}/features.xlsx')

            # Number of columns to plot (adjusted to avoid the first two)
            num_cols = len(df.columns) - 2

            # Determine the grid size for subplots
            num_rows = (num_cols // 6) + 1 if num_cols % 6 != 0 else num_cols // 6
            fig, axes = plt.subplots(nrows=num_rows, ncols=6, figsize=(60, 70))

            # Flatten axes to easily iterate over them (in case of multiple rows/columns)
            axes = axes.flatten()

            # Plot each column as a separate subplot
            for i, column in enumerate(df.columns[2:]):  # Start from 2 to skip the first two columns
                axes[i].plot(df[column].values)  # Explicitly use index and column values
                axes[i].set_title(f'Column: {column}')  # Set title to column name
                axes[i].set_xlabel('Index')
                axes[i].set_ylabel(column)

            # Hide any unused subplots
            for j in range(i + 1, len(axes)):
                fig.delaxes(axes[j])

            # Adjust layout to prevent overlap
            plt.tight_layout()

            # Save each plot to a file
            plt.savefig(f'/home/robin/anomaly_detection_process_data/Masterflex-KI/63_new/{cur_subj}/plot.png')

            # Display the plot
            plt.show()


            # new_df = finalize_df(df)
            # if len(df) > max_num:
            #     print(cur_subj)
            #     print(len(df))
            #     max_num = len(df)
            # new_df.to_csv(f'/home/robin/anomaly_detection_process_data/Masterflex-KI/63_new/{cur_subj}/features.csv', index=False)
            # new_df.to_excel(f'/home/robin/anomaly_detection_process_data/Masterflex-KI/63_new/{cur_subj}/features.xlsx',index=False)
        # else:
        #     print(cur_subj + ' exists')

# main_context()