import os
import matplotlib.cm as cm
import matplotlib.patches as mpatches
import numpy as np
from sklearn.ensemble import IsolationForest
import matplotlib.pyplot as plt
import pandas as pd
from utils import *
from preprocess import load_and_preprocess  # Assuming preprocess.py exists and contains load_and_preprocess function
import shap

from sklearn.preprocessing import StandardScaler

class largeIsolationForestTrainer():
    def __init__(self,
                 path='data',
                 files='seq-13',
                 num_clusters=6):
        self.path = path
        self.files = files
        self.files_train = self.files[:-8]
        self.files_test = self.files[-8:]
        self.scaler =  StandardScaler()
        self.optimal_clusters = num_clusters

    def get_data(self, files):
        train_df = pd.DataFrame()  # Initialize an empty DataFrame to store all data
        for cur_file in files:
            cur_df = pd.read_csv(os.path.join(self.path, cur_file, 'features.csv'))
            if not cur_df.isnull().values.any():
                train_df = pd.concat([train_df, cur_df], ignore_index=True)
        train_df.drop(train_df.columns[-4:-1], axis=1, inplace=True)
        return train_df

    def run_isolationforest_with_outlier_detection(self):
        scaled_features = self.scaler.fit_transform(self.train_df)

        contamination = 0.1



            # Create an Isolation Forest model
        self.isolation_forest = IsolationForest(n_estimators=400, max_samples='auto',
                                               contamination=contamination, max_features=13,
                                               bootstrap=False, n_jobs=-1, random_state=42)

            # Fit the model to the scaled features
        self.isolation_forest.fit(scaled_features)

        predictions = self.isolation_forest.predict(scaled_features)

        self.train_df['is_outlier'] = predictions




    def test_kmeans_with_outlier_detection(self, df):
        scaled_features = self.scaler.transform(df)
        df['is_outlier'] = self.isolation_forest.predict(scaled_features)



        return df



    def plot_single_seq(self,df, thresh, figtitle, save=False):
        scaled_features = self.scaler.transform(df[df.columns[:-3]])

        df['cluster'] = self.kmeans.predict(scaled_features)
        distances = np.min(self.kmeans.transform(scaled_features), axis=1)
        df['distance'] = distances
        df['is_outlier'] = distances > thresh

        if save:
            new_feat = df.copy()
            if not os.path.exists('Masterflex-KI/new_sequences/Results_kmeans/' + str(figtitle)):
                os.mkdir('Masterflex-KI/new_sequences/Results_kmeans/' + str(figtitle))
            new_feat.to_excel('Masterflex-KI/new_sequences/Results_kmeans/' + str(figtitle) + '/Results' + '.xlsx')


        std_norm = []
        df_col = []
        for cur_column in df.columns[:-4]:
            cur_std = df[cur_column].std()
            if cur_std != 0:
                std_norm.append(df[cur_column].std() / df[cur_column].mean())
                df_col.append(cur_column)
            else:
                std_norm.append(0)
                df_col.append(cur_column)

        sorted_indices = sorted(range(len(std_norm)), key=lambda i: std_norm[i], reverse=True)
        highest_std_indices = sorted_indices[:6]

        six_highest_std = [std_norm[i] for i in highest_std_indices]
        six_highest_col_names = [df_col[i] for i in highest_std_indices]

        fig, axs = plt.subplots(2, 3, figsize=(15, 10))
        for ax, cur_name in zip(axs.flat, six_highest_col_names):
            ax.plot(df['seconds'], df[cur_name])
            it = 0
            for index, row in df.iterrows():
                if row['is_outlier']:
                    it += 1
                    ax.plot([row['seconds']], row[cur_name], 'ro', color='red')
            ax.set_xlabel('Seconds')
            ax.set_title('Feature: ' + cur_name, fontsize=9)

        fig.suptitle(figtitle, fontsize=15)

        if save:
            plt.savefig('Masterflex-KI/Results_kmeans/' + figtitle + '.png')
        plt.show()

    def plot_single_seq2(self, df, thresh, figtitle, save=False):
        scaled_features = self.scaler.transform(df[df.columns[:-1]])

        df['is_outlier'] = self.isolation_forest.predict(scaled_features)



        # Define colormap
        cmap = cm.get_cmap('coolwarm')

        if save:
            new_feat = df.copy()
            if not os.path.exists('Masterflex-KI/new_sequences/Results_kmeans/' + str(figtitle)):
                os.mkdir('Masterflex-KI/new_sequences/Results_kmeans/' + str(figtitle))
            new_feat.to_excel('Masterflex-KI/new_sequences/Results_kmeans/' + str(figtitle) + '/Results' + '.xlsx')

        std_norm = []
        df_col = []
        for cur_column in df.columns[:-4]:
            cur_std = df[cur_column].std()
            if cur_std != 0:
                std_norm.append(df[cur_column].std() / df[cur_column].mean())
                df_col.append(cur_column)
            else:
                std_norm.append(0)
                df_col.append(cur_column)

        sorted_indices = sorted(range(len(std_norm)), key=lambda i: std_norm[i], reverse=True)
        highest_std_indices = sorted_indices[:6]

        six_highest_std = [std_norm[i] for i in highest_std_indices]
        six_highest_col_names = [df_col[i] for i in highest_std_indices]

        fig, axs = plt.subplots(2, 3, figsize=(15, 10))
        for ax, cur_name in zip(axs.flat, six_highest_col_names):
            ax.plot(df['seconds'], df[cur_name])
            it = 0
            for index, row in df.iterrows():
                if row['is_outlier']:
                    it += 1
                    # Calculate color based on distance
                    # color = cmap((row['distance'] - thresh) / (max(distances) - thresh))
                    # Control size of plotted dots
                    # dot_size = 10 + row['distance'] * 2  # Adjust multiplier as needed
                    ax.plot([row['seconds']], row[cur_name], 'o', color='red', markersize=2)
            ax.set_xlabel('Seconds')
            ax.set_title('Feature: ' + cur_name, fontsize=9)

        fig.suptitle(figtitle, fontsize=15)

        # Add color legend
        legend_handles = []
        for distance_value in np.linspace(0, 15, 10):
            color = cmap((distance_value - thresh) / (15 - thresh))
            legend_handles.append(mpatches.Patch(color=color, label=f'{distance_value:.2f}'))

        fig.legend(handles=legend_handles, title='Distance from Threshold', loc='upper right')

        if save:
            plt.savefig('Masterflex-KI/new_sequences/Results_kmeans/' + str(figtitle) + '/plot.png')
        plt.show()



def main():
    path = 'Masterflex-KI/new_sequences'
    #'6', '7', '8', '10', '11', '12', '13', '14', '15', '16', '24', '31',
    files = ['32', '33', '41',
             '42', '43', '44', '45', '46', '47', '48', '49', '50', '51', '52', '139', '140',
             '142', '143', '144', '145', '146', '147', '148', '149', '150', '151', '152', '153',
             '154', '155', '156', '157', '158', '166', '167', '168', '169', '170']
    self = largeIsolationForestTrainer(path, files)

    self.train_df = self.get_data(self.files_train)
    # self.find_optimal_clusters()
    self.run_isolationforest_with_outlier_detection()

    for cur_test in self.files_test:
        if not os.path.exists('Masterflex-KI/new_sequences/Results_isolation_forest/' + cur_test):
            print(cur_test)
            cur_test_df = self.get_data([cur_test])
            cur_test_df = self.test_kmeans_with_outlier_detection(cur_test_df)
            # self.plot_tsne(cur_test_df.copy())
            self.plot_single_seq2(cur_test_df.copy(), thresh=5, figtitle=cur_test, save=True)
    for cur_train in self.files_train:
        cur_train_df = self.get_data([cur_train])
        cur_train_df = self.test_kmeans_with_outlier_detection(cur_train_df)
        # self.plot_tsne(cur_train_df.copy())
        self.plot_single_seq2(cur_train_df.copy(), thresh=5, figtitle=cur_train, save=True)


# main()