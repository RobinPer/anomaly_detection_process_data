import pandas as pd
from torch.utils.data import Dataset, DataLoader
import torch


class SensorDataset(Dataset):
    def __init__(self, sequence_ids, base_path):
        self.sequence_ids = sequence_ids
        self.base_path = base_path

    def __len__(self):
        return len(self.sequence_ids)

    def __getitem__(self, idx):
        # Define the columns for input and target
        x_train = ['0dbdbfbe-69a3-4ba6-b873-8893bca976a7', '5e3053f8-6d41-45ce-94e9-11e069f775a5',
                   'e72186fc-d581-4368-917d-92ebd8124e5c', '2a4433b9-0f99-4cc2-9669-0b17eeaa62ab',
                   '9265f861-e03e-4697-a132-ebfd93004827', '6b0c06fb-c6c8-49bc-a422-5b1ba2f7e4bc',
                   'cf7314c0-c20c-4c53-b48f-debf29c87b43', '59997204-2b97-478b-9120-d1b3d784ad4d',
                   '2acf6ab4-7288-4750-8c75-e04169ffc51e', '09545925-7c3e-4619-afe8-9b8ef5b01f24',
                   '33d2504f-7719-43f0-9c1f-9a5dae89bc24', 'a36d9a05-6c14-4abf-aae9-4f9d09237a4d',
                   '1310aedc-bcd7-429b-8aa9-2ed81d7533df', '5f22224c-8b0f-4985-bbf9-f5b01fda8d6b',
                   'b16f6189-3804-4463-96fb-4bd444d0c276', '6289cd82-4bb2-4488-8975-4d2729bdfa9d',
                   'bbb36936-1a05-406b-9178-2b790ca07a5d', '777f6918-12a2-4629-be75-5a669872f125',
                   '50bfd169-8434-48b2-bf28-0a9665efdd84', 'f61fe432-3704-49cf-9559-ece9a1d45c5a',
                   'eda51693-36df-4c68-8532-55bb21990351', '852d5936-7606-4bec-bbaf-9e92fa8b6145',
                   '4d78ad32-b33b-4022-b6bc-4ba11e4544ef', '348b29c9-4a41-4efe-938f-3b54c2297c9f',
                   '5101caad-35df-4d7f-8bce-80fefa4df866', '02ce84e5-d56f-40fd-8025-4dd85f5e2924',
                   '45c84756-6a09-4040-80c6-15993de05bd4', '1115a47d-f41b-43e9-9804-3fa2f809b54a',
                   'ed42f03f-09f6-4906-888e-10afd7871409', '30babb2b-84d1-4d7e-860f-2c1eae9ae177']

        y = ['c3677f59-6137-4e8f-97f3-077ed6a62f2a', 'b37fd9cb-d968-4004-8afc-80a3522048a5',
             '870e0de0-2235-43bf-b309-101e6dfad86f', 'd08e8aba-87ad-47c6-b373-de74d73a351e']

        sequence_id = self.sequence_ids[idx]
        file_path = f"{self.base_path}/{sequence_id}/features.csv"

        # Load the CSV file
        df = pd.read_csv(file_path, low_memory=False)
        df.ffill(inplace=True)

        # Separate inputs and targets
        inputs = torch.tensor(df[x_train].values, dtype=torch.float32)
        targets = torch.tensor(df[y].values, dtype=torch.float32)

        # Debugging print statements
        print(f"Inputs shape: {inputs.shape}")
        print(f"Targets shape: {targets.shape}")

        return inputs, targets
