import pandas as pd
from pyts.preprocessing import StandardScaler
from sklearn.model_selection import train_test_split
from sklearn.ensemble import RandomForestRegressor
from xgboost import XGBRegressor
from sklearn.preprocessing import StandardScaler, LabelEncoder
from sklearn.ensemble import GradientBoostingClassifier as GBC
from sklearn.metrics import mean_squared_error, mean_absolute_error
import numpy as np
import matplotlib.pyplot as plt
import random
from utils import *


def main():
    # Train Model on these features
    x_train = ['0dbdbfbe-69a3-4ba6-b873-8893bca976a7', '5e3053f8-6d41-45ce-94e9-11e069f775a5',
               'e72186fc-d581-4368-917d-92ebd8124e5c', '2a4433b9-0f99-4cc2-9669-0b17eeaa62ab',
               '9265f861-e03e-4697-a132-ebfd93004827', '6b0c06fb-c6c8-49bc-a422-5b1ba2f7e4bc',
               'cf7314c0-c20c-4c53-b48f-debf29c87b43', '59997204-2b97-478b-9120-d1b3d784ad4d',
               '2acf6ab4-7288-4750-8c75-e04169ffc51e', '09545925-7c3e-4619-afe8-9b8ef5b01f24',
               '33d2504f-7719-43f0-9c1f-9a5dae89bc24', 'a36d9a05-6c14-4abf-aae9-4f9d09237a4d',
               '1310aedc-bcd7-429b-8aa9-2ed81d7533df', '5f22224c-8b0f-4985-bbf9-f5b01fda8d6b',
               'b16f6189-3804-4463-96fb-4bd444d0c276', '6289cd82-4bb2-4488-8975-4d2729bdfa9d',
               'bbb36936-1a05-406b-9178-2b790ca07a5d', '777f6918-12a2-4629-be75-5a669872f125',
               '50bfd169-8434-48b2-bf28-0a9665efdd84', 'f61fe432-3704-49cf-9559-ece9a1d45c5a',
               'eda51693-36df-4c68-8532-55bb21990351', '852d5936-7606-4bec-bbaf-9e92fa8b6145',
               '4d78ad32-b33b-4022-b6bc-4ba11e4544ef', '348b29c9-4a41-4efe-938f-3b54c2297c9f',
               '5101caad-35df-4d7f-8bce-80fefa4df866', '02ce84e5-d56f-40fd-8025-4dd85f5e2924',
               '45c84756-6a09-4040-80c6-15993de05bd4', '1115a47d-f41b-43e9-9804-3fa2f809b54a',
               'ed42f03f-09f6-4906-888e-10afd7871409', '30babb2b-84d1-4d7e-860f-2c1eae9ae177',
               'seconds']
    x_label = [
        'b0a51b71-7ba0-49a4-b2a2-13984d8c6616']  # , 'f022cf4f-dc76-4196-a41d-654ed24b2aa1', 'd85d28ca-fb7c-4ec3-951a-7972e5b62925']
    x_adaptable = ['5e3053f8-6d41-45ce-94e9-11e069f775a5', 'e72186fc-d581-4368-917d-92ebd8124e5c',
                   '2a4433b9-0f99-4cc2-9669-0b17eeaa62ab', '6b0c06fb-c6c8-49bc-a422-5b1ba2f7e4bc',
                   'cf7314c0-c20c-4c53-b48f-debf29c87b43', '59997204-2b97-478b-9120-d1b3d784ad4d',
                   '2acf6ab4-7288-4750-8c75-e04169ffc51e', '09545925-7c3e-4619-afe8-9b8ef5b01f24',
                   '33d2504f-7719-43f0-9c1f-9a5dae89bc24', '1310aedc-bcd7-429b-8aa9-2ed81d7533df',
                   'bbb36936-1a05-406b-9178-2b790ca07a5d', '777f6918-12a2-4629-be75-5a669872f125',
                   '50bfd169-8434-48b2-bf28-0a9665efdd84', 'eda51693-36df-4c68-8532-55bb21990351',
                   '4d78ad32-b33b-4022-b6bc-4ba11e4544ef', '348b29c9-4a41-4efe-938f-3b54c2297c9f',
                   '5101caad-35df-4d7f-8bce-80fefa4df866', '02ce84e5-d56f-40fd-8025-4dd85f5e2924',
                   '45c84756-6a09-4040-80c6-15993de05bd4', '1115a47d-f41b-43e9-9804-3fa2f809b54a',
                   'ed42f03f-09f6-4906-888e-10afd7871409']
    y = ['c3677f59-6137-4e8f-97f3-077ed6a62f2a', 'b37fd9cb-d968-4004-8afc-80a3522048a5',
         '870e0de0-2235-43bf-b309-101e6dfad86f', 'd08e8aba-87ad-47c6-b373-de74d73a351e']  # target outputs

    # sequence_ids = ['MO112401', 'MO109301', 'MO111618', 'MO109174', 'MO111915', 'MO108058', 'MO109011', 'MO111624',
    #                 'MO112101', 'MO107772', 'MO109302', 'MO109161', 'MO109149', 'MO109068', 'MO109308', 'MO109914',
    #                 'MO109345', 'MO108912', 'MO109170', 'MO109089', 'MO109515', 'MO111365', 'MO109237', 'MO108801',
    #                 'MO109601', 'MO112102', 'MO109241', 'MO108980', 'MO111450', 'MO109759', 'MO109086', 'MO111616',
    #                 'MO107892', 'MO109169', 'MO108466', 'MO109586', 'MO111531', 'MO109409', 'MO109251', 'MO111867',
    #                 'MO108467', 'MO108799', 'MO109181', 'MO108792', 'MO109090', 'MO109745', 'MO108736', 'MO111630',
    #                 'MO109529', 'MO109091', 'MO108465', 'MO108794', 'MO111617', 'MO109805', 'MO111708', 'MO109171',
    #                 'MO109163', 'MO109830', 'MO108800', 'MO109407', 'MO111866', 'MO109758', 'MO108468', 'MO111591',
    #                 'MO109183', 'MO108384', 'MO109151', 'MO109077', 'MO109248', 'MO108795', 'MO109249', 'MO109176',
    #                 'MO111364', 'MO109425', 'MO108731', 'MO109087', 'MO109073', 'MO108874', 'MO112254', 'MO109162',
    #                 'MO109496', 'MO108668', 'MO109175', 'MO109424', 'MO109240', 'MO108544', 'MO109528', 'MO108737',
    #                 'MO112176']

    sequence_ids = ['MO108332', 'MO108401', 'MO108534', 'MO108902', 'MO108980', 'MO109073', 'MO109161', 'MO109162',
                    'MO109301', 'MO109345', 'MO109407', 'MO109528', 'MO109529', 'MO109601', 'MO109758', 'MO111364',
                    'MO111365', 'MO111450', 'MO111616', 'MO111617', 'MO111618', 'MO111866', 'MO111915', 'MO112101',
                    'MO112176']


    random.seed = 42
    # Split into train, validation, and test sets (e.g., 70% train, 20% validation, 10% test)
    random.shuffle(sequence_ids)  # Shuffle sequences for random splitting
    test_size = int(0.1 * len(sequence_ids))
    train_size = len(sequence_ids) - test_size

    train_sequence_ids = sequence_ids[:train_size]
    test_sequence_ids = sequence_ids[train_size:]

    data_train, data_train_y = create_dataset(train_sequence_ids,
                                              x_train,
                                              x_label,
                                              y)


    label_encoder = LabelEncoder()
    data_train[x_label[0]] = label_encoder.fit_transform(data_train[x_label[0]])
    # data_test, data_test_y = create_dataset(['MO112102', 'MO112176', 'MO112254', 'MO112401'],
    data = data_train.values
    data_y = data_train_y.values


    data, data_y = create_temporal_dataset(data, data_y)

    n_samples, n_time_steps, n_features = data.shape
    data = data.reshape(n_samples, -1)  # Flatten to 2D for scaling

    # Define all features (both x_train and x_label here)
    all_features = x_train + x_label

    # Apply feature engineering to the train and test datasets
    data_train = create_features(data_train, features=all_features)

    label_encoder = LabelEncoder()
    data_train[x_label[0]] = label_encoder.fit_transform(data_train[x_label[0]])
    # data_test, data_test_y = create_dataset(['MO112102', 'MO112176', 'MO112254', 'MO112401'],


    scaler_train = StandardScaler()
    scaler_y = StandardScaler()

    data_train_scaled = scaler_train.fit_transform(data_train)
    data_train_y_scaled = scaler_y.fit_transform(data_train_y)

    # Initialize and train the RandomForestRegressor
    rf_regressor = XGBRegressor(random_state=42, n_estimators=800)
    rf_regressor.fit(data_train_scaled, data_train_y_scaled)

    # Make predictions and evaluate

    for cur_test_seq in test_sequence_ids:
        data_test, data_test_y = create_dataset([cur_test_seq],
                                                x_train,
                                                x_label,
                                                y)
        # data_test = create_features(data_test, features=all_features)

        data_test[x_label[0]] = np.max(data_train[x_label[0]]) + 1
        data_test_scaled = scaler_train.transform(data_test)
        data_test_y_scaled = scaler_y.transform(data_test_y)

        y_pred_scaled = rf_regressor.predict(data_test_scaled)
        y_pred_real = scaler_y.inverse_transform(y_pred_scaled)
        y_test_real = scaler_y.inverse_transform(data_test_y_scaled)
        mse = mean_squared_error(y_test_real, y_pred_real)
        mae = mean_absolute_error(y_test_real, y_pred_real)

        print(f"XGBRegressor Mean Squared Error: {mse:.4f}")
        print(f"XGBRegressor Mean Absolut Error: {mae:.4f}")

        plot_pred(y_pred_real, y_test_real, cur_test_seq)

    # target_parameters = pd.DataFrame([[78, 13, 86, 13]], columns=y)
    # target_parameters_scalede = scaler_y.transform(target_parameters)
    # current_features = data_test[0]
    print('Done')

# main()